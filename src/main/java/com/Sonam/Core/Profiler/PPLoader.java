package com.Sonam.Core.Profiler;

import java.util.ArrayList;
import java.util.UUID;

@SuppressWarnings("unchecked")
public class PPLoader implements Runnable {

    private ProxyProfile proxyProfile;

    public PPLoader(ProxyProfile proxyProfile) {
        this.proxyProfile = proxyProfile;
    }

    public void run() {
        ProfileApi api = proxyProfile.getNewApi();

        proxyProfile.setRank(api.getRank());
        proxyProfile.setPackageRank(api.getPackageRank());
        proxyProfile.setUsername(api.getString("username"));
        if(api.getString("prefix") != null) {
            proxyProfile.setPrefix(api.getString("prefix"));
        } else if(api.getRank().equals(Rank.NORMAL)) {
            proxyProfile.setPrefix(proxyProfile.getPackageRank().getPrefix());
        } else {
            proxyProfile.setPrefix(proxyProfile.getRank().getPrefix());
        }
        proxyProfile.setPackages((ArrayList<String>) api.get("packages"));
        proxyProfile.setMuted(api.isMuted());
        proxyProfile.setGuild(api.getGuildID());
        proxyProfile.setFriendRequests((ArrayList<UUID>) api.get("friendRequests"));

    }

}

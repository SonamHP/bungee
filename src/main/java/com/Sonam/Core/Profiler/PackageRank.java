package com.Sonam.Core.Profiler;

public enum PackageRank {

    NORMAL(0, "&7"),
    VIP(1, "&a[VIP]"),
    VIP_PLUS(2, "&a[VIP&6+&a]"),
    MVP(3, "&b[MVP]"),
    MVP_PLUS(4, "&b[MVP&c+&b]");

    int level;
    String prefix;

    PackageRank(int level, String prefix) {
        this.level = level;
        this.prefix = prefix;
    }

    public int getLevel() {
        return level;
    }

    public String getPrefix() {
        return prefix;
    }

}

package com.Sonam.Core.Profiler;

import java.util.ArrayList;
import java.util.UUID;

public class ProxyProfile {

    private P_API api;
    private ProfileApi newApi;
    private UUID uuid;
    private String username;
    private String prefix;
    private Rank rank;
    private PackageRank packageRank;
    private ArrayList<String> packages;
    private ArrayList<UUID> friendRequests;
    private boolean muted;
    private UUID guild;

    public ProxyProfile(UUID uuid) {
        this.uuid = uuid;

        api = new P_API(uuid);
        newApi = new ProfileApi(uuid);
    }

    public P_API getApi() {
        return api;
    }

    public ProfileApi getNewApi() {
        return newApi;
    }

    public UUID getUniqueId() {
        return uuid;
    }

    public String getUsername(){
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public PackageRank getPackageRank() {
        return packageRank;
    }

    public void setPackageRank(PackageRank packageRank) {
        this.packageRank = packageRank;
    }

    public ArrayList<String> getPackages() {
        return packages;
    }

    public void setPackages(ArrayList<String> packages) {
        this.packages = packages;
    }

    public UUID getGuild() {
        return guild;
    }

    public void setGuild(UUID guild) {
        this.guild = guild;
    }

    public void setMuted(boolean muted) { this.muted = muted; }

    public boolean isMuted() { return muted; }

    public void setFriendRequests(ArrayList<UUID> friendRequests) {
        this.friendRequests = friendRequests;
    }

    public ArrayList<UUID> getFriendRequests() {
        return friendRequests;
    }

    public void addFriendRequest(UUID uuid) {
        getFriendRequests().add(uuid);
    }

    public void removeFriendRequest(UUID uuid) {
        getFriendRequests().remove(uuid);
    }

}

package com.Sonam.Core.Profiler;

import com.Sonam.Core.Core;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class PPManager {

    private Core plugin;

    public PPManager(Core plugin) {
        this.plugin = plugin;
    }

    private Set<ProxyProfile> profiles = new HashSet<ProxyProfile>();

    public ProxyProfile getProfile(UUID uuid) {
        for(ProxyProfile profile : profiles) {
            if(profile.getUniqueId().equals(uuid)) {
                return profile;
            }
        }
        return null;
    }

    public Set<ProxyProfile> getProfiles() {
        return profiles;
    }

    public void createProfile(UUID uuid) {
        ProxyProfile profile = new ProxyProfile(uuid);
        plugin.getProxy().getScheduler().runAsync(plugin, new PPLoader(profile));
        profiles.add(profile);
    }

    public void unloadProfile(UUID uuid) {
        if(getProfiles().contains(getProfile(uuid))) {
            profiles.remove(getProfile(uuid));
        }
    }

}

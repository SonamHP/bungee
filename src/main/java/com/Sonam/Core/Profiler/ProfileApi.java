package com.Sonam.Core.Profiler;

import com.Sonam.Core.Core;
import com.mongodb.client.MongoCollection;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SuppressWarnings("unchecked")
public class ProfileApi {

    private UUID uuid;
    private MongoCollection<Document> collection;
    private List<Document> results;
    private Document result;

    public ProfileApi(UUID uuid) {
        this.uuid = uuid;
        collection = Core.getMongo().getDatabaseNew().getCollection("profiles");

        results = collection.find(new Document("uniqueId", uuid)).into(new ArrayList<Document>());
        if(!results.isEmpty()) {
            result = results.get(0);
        }
    }

    public String getString(String field) {
        if(result.get(field) != null) {
            return result.getString(field);
        }
        return null;
    }

    public int getInteger(String field) {
        if(result.getInteger(field) != null) {
            return result.getInteger(field);
        }
        return 0;
    }

    public UUID getUUIDObject(String field) {
        return (UUID) result.get(field);
    }

    public UUID getGuildID() {
        if(result.get("guild") != null) {
            return (UUID) result.get("guild");
        }
        return null;
    }

    public Object get(String field) {
        if(result.get(field) != null) {
            return result.get(field);
        }
        return null;
    }

    public PackageRank getPackageRank() {
        if(result.getString("package_rank") != null) {
            String rank = result.getString("package_rank");
            for(PackageRank packageRank : PackageRank.values()) {
                if(packageRank.name().equalsIgnoreCase(rank)) {
                    return packageRank;
                }
            }
        }
        return null;
    }

    public Rank getRank() {
        if(result.getString("rank") != null) {
            String rank = result.getString("rank");
            for(Rank ranktype : Rank.values()) {
                if(ranktype.name().equalsIgnoreCase(rank)) {
                    return ranktype;
                }
            }
        }
        return null;
    }

    public String getPrefix() {
        if(result.getString("prefix") != null) {
            return result.getString("prefix");
        } else if(result.get("rank").equals("NORMAL")) {
            return getPackageRank().getPrefix();
        } else {
            return getRank().getPrefix();
        }
    }

    public boolean isMuted() {
        if(result.get("mutes") != null) {
            ArrayList<Document> mutes = (ArrayList<Document>) result.get("mutes");
            if(mutes.isEmpty()) {
                return false;
            }
            Document document = mutes.get(mutes.size() - 1);
            return (Boolean) document.get("active");
        }
        return false;
    }

    public void update(String field, Object value) {
        collection.findOneAndUpdate(new Document("uniqueId", uuid), new Document("$set", new Document(field, value)));
    }

    public boolean exists() {
        return !results.isEmpty();
    }

    public void addFriendRequest(UUID uuid) {
        ArrayList<UUID> requests = (ArrayList<UUID>) result.get("friendRequests");
        requests.add(uuid);
        collection.findOneAndUpdate(new Document("uniqueId", uuid), new Document("$set", new Document("friendRequests", requests)));
    }

    public void removeFriendRequest(UUID uuid) {
        ArrayList<UUID> requests = (ArrayList<UUID>) result.get("friendRequests");
        requests.remove(uuid);
        collection.findOneAndUpdate(new Document("uniqueId", uuid), new Document("$set", new Document("friendRequests", requests)));
    }

    public boolean isFullDefault() {
        if(getPackageRank().equals(PackageRank.NORMAL) && getRank().equals(Rank.NORMAL)) {
            return true;
        }
        return false;
    }


}

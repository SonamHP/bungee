package com.Sonam.Core.Profiler;

public enum Rank {

    NORMAL(false, 0, "", "&fNORMAL"),
    HELPER(true, 2, "&9[HELPER]", "&9HELPER"),
    YOUTUBER(false, 1, "&6[YT]", "&6YOUTUBER"),
    MODERATOR(true, 3, "&2[MOD]", "&2MODERATOR"),
    ADMIN(true, 4, "&c[ADMIN]", "&cADMIN");

    boolean staff;
    int level;
    String prefix;
    String setter;

    Rank(boolean staff, int level, String prefix, String setter) {
        this.staff = staff;
        this.level = level;
        this.prefix = prefix;
        this.setter = setter;
    }

    public boolean isStaff() {
        return staff;
    }

    public int getLevel() {
        return level;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getSetter() {
        return setter;
    }


}
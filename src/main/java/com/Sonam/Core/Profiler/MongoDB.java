package com.Sonam.Core.Profiler;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

@SuppressWarnings("deprecation")
public class MongoDB {

    private MongoClient mongo = null;
    private DB database = null;
    private MongoDatabase databaseNew = null;

    public MongoClient getMongo() {
        if(mongo == null) {
            this.mongo = new MongoClient();
        }
        return this.mongo;
    }

    public MongoDB() {
        this.mongo = new MongoClient();
    }

    public DB getDatabase() {
        if(this.database == null) {
            this.database = getMongo().getDB("main_server");
        }
        return this.database;
    }

    public MongoDatabase getDatabaseNew() {
        if(this.databaseNew == null) {
            this.databaseNew = getMongo().getDatabase("main_server");
        }
        return this.databaseNew;
    }

    public void setDatabase(String db) {
        this.database = getMongo().getDB(db);
    }

    public void setDatabaseNew(String databaseNew) {
        this.databaseNew = getMongo().getDatabase(databaseNew);
    }

    public void closeConnection() {
        if(this.mongo != null) {
            this.mongo.close();
        }
    }

}

package com.Sonam.Core.Profiler;

import com.Sonam.Core.Core;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import org.bson.Document;

import java.util.UUID;

public class P_API {

    private UUID uuid;
    private DBCollection collection;
    private DBCollection cache;
    private BasicDBObject query;
    private DBObject result;
    private DBObject resultcache;

    public P_API(UUID uuid) {
        this.uuid = uuid;

        collection = Core.getMongo().getDatabase().getCollection("profiles");
        cache = Core.getMongo().getDatabase().getCollection("cache");

        query = new BasicDBObject();
        query.put("uniqueId", uuid);
        result = collection.findOne(query);
        resultcache = cache.findOne(query);

    }

    public String getString(String field) {
        if(result.get(field) != null) {
            return (String) result.get(field);
        }
        return null;
    }

    public Boolean getBoolean(String field) {
        if(result.get(field) != null) {
            return (Boolean) result.get(field);
        }
        return null;
    }

    public Integer getInt(String field) {
        if(result.get(field) != null) {
            return (Integer) result.get(field);
        }
        return null;
    }

    public Object get(String field) {
        if(result.get(field) != null) {
            return result.get(field);
        }
        return null;
    }

    public Rank getRank() {
        if(result.get("rank") != null) {
            String rank = (String) result.get("rank");
            for(Rank ranktype : Rank.values()) {
                if(ranktype.name().equalsIgnoreCase(rank)) {
                    return ranktype;
                }
            }
        }
        return null;
    }

    public BasicDBList getPackages() {
        if(result.get("packages") != null) {
            return (BasicDBList) result.get("packages");
        }
        return null;
    }

    public PackageRank getPackageRank() {
        if(result.get("package_rank") != null) {
            String rank = (String) result.get("package_rank");
            for(PackageRank packageRank : PackageRank.values()) {
                if(packageRank.name().equalsIgnoreCase(rank)) {
                    return packageRank;
                }
            }
        }
        return null;
    }

    public String getPrefix() {
        if(result.get("prefix") != null) {
            return (String) result.get("prefix");
        } else if(result.get("rank").equals("NORMAL")) {
            return getPackageRank().getPrefix();
        } else {
            return getRank().getPrefix();
        }
    }

    public void update(String key, Object value) {
        BasicDBObject updated = new BasicDBObject();
        updated.put(key, value);
        collection.update(query, new BasicDBObject().append("$set", updated));
    }

    public void updateCachePrefix(String value) {
        BasicDBObject updated = new BasicDBObject();
        updated.put("prefix", value);
        cache.update(query, new BasicDBObject().append("$set", updated));

        for(Document object : Core.getCache()) {
            if(object.get("uniqueId").equals(uuid)) {
                object.put("prefix", value);
            }
        }
    }

}

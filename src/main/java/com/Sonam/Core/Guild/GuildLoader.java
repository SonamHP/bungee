package com.Sonam.Core.Guild;

import com.Sonam.Core.Core;
import com.mongodb.BasicDBList;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.client.MongoCollection;
import net.md_5.bungee.api.ChatColor;
import org.bson.Document;

import java.util.ArrayList;
import java.util.UUID;

@SuppressWarnings("unchecked")
public class GuildLoader implements Runnable {

    public void run() {
        MongoCollection<Document> collection = Core.getMongo().getDatabaseNew().getCollection("guilds");
        ArrayList<Document> cursor = collection.find().into(new ArrayList<Document>());

        if (cursor.isEmpty()) {
            Core.getPlugin().getProxy().getLogger().info(ChatColor.translateAlternateColorCodes('&', "&a[CORE] &fNo guilds found!"));
            return;
        }

        for(Document result : cursor) {
            try {
                String name = result.getString("name");
                UUID owner = (UUID) result.get("master");
                ArrayList<Document> members = (ArrayList<Document>) result.get("members");
                String tag = result.getString("tag");

                Thread.sleep(100);

                Core.getGuildManager().createGuild(name, owner, members, tag);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Core.getPlugin().getProxy().getLogger().info(ChatColor.translateAlternateColorCodes('&', "&a[CORE] &fGuilds Loaded!"));
    }
}

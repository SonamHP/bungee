package com.Sonam.Core.Guild;

import com.Sonam.Core.Core;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import java.util.UUID;

public class G_API {

    private UUID master;
    private DBCollection collection;
    private BasicDBObject query;
    private DBObject result;

    public G_API(UUID master) {
        this.master = master;

        collection = Core.getMongo().getDatabase().getCollection("guilds");
        query = new BasicDBObject("master", master);
        result = collection.findOne(query);
    }

    public String getString(String field) {
        if(result.get(field) != null) {
            return (String) result.get(field);
        }
        return null;
    }

    public UUID getMaster() {
        if(result.get("master") != null) {
            return (UUID) result.get("master");
        }
        return null;
    }

    public BasicDBList getMembers() {
        if(result.get("members") != null) {
            return (BasicDBList) result.get("members");
        }
        return null;
    }




}

package com.Sonam.Core.Guild;

import com.Sonam.Core.Core;
import com.mongodb.BasicDBList;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import org.bson.Document;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.UUID;

public class Guild {

    private UUID master;
    private String name;
    private String tag;
    private BasicDBList m;
    private ArrayList<Document> mf;

    public Guild(UUID master, String name, String tag, ArrayList<Document> mf) {
        this.master = master;
        this.name = name;
        this.tag = tag;
        this.mf = mf;
    }

    public String getName() {
        return name;
    }

    public UUID getMaster() {
        return master;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public void setMembers(ArrayList<Document> mf) {
        this.mf = mf;
    }

    public ArrayList<Document> getMembers() {
        return mf;
    }

    public Document getMember(UUID uuid) {
        for(Document m : mf) {
            if(m.get("uuid").equals(uuid)) {
                return m;
            }
        }
        return null;
    }

    public int getMemberRank(UUID uuid) {
        for(Document m : mf) {
            if(m.get("uuid").equals(uuid)) {
                if(m.getString("role").equalsIgnoreCase("officer")) {
                    return 1;
                }
                if(m.getString("role").equalsIgnoreCase("master")) {
                    return 2;
                }
            }
        }
        return 0;
    }

    public void sendMessage(String message) {
        for(Document member : getMembers()) {
            UUID uuid = (UUID) member.get("uuid");
            if(Core.getPlayer(uuid) != null){
                Core.getPlayer(uuid).sendMessage(TextComponent.fromLegacyText(message));
            }
        }
    }

    public void sendMessageFormatted(String message) {
        for(Document member : getMembers()) {
            UUID uuid = (UUID) member.get("uuid");
            if(Core.getPlayer(uuid) != null){
                Core.getPlayer(uuid).sendMessage(TextComponent.fromLegacyText(ChatColor.AQUA + "-----------------------------------------------------\n" + ChatColor.RESET +
                        message + "\n" +
                        ChatColor.AQUA + "-----------------------------------------------------"
                ));
            }
        }
    }
}

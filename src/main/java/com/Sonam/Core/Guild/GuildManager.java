package com.Sonam.Core.Guild;

import com.Sonam.Core.Core;
import com.mongodb.BasicDBList;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import org.bson.Document;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class GuildManager {

    private Set<Guild> guilds = new HashSet<Guild>();

    public Guild getGuild(UUID owner) {
        for(Guild guild : guilds) {
            if(guild.getMaster().equals(owner)) {
                return guild;
            }
        }
        return null;
    }

    public Set<Guild> getGuilds() {
        return guilds;
    }

    public void createGuild(String name, UUID master, ArrayList<Document> members, String tag) {
        Guild guild = new Guild(master, name, tag, members);
        guilds.add(guild);
    }

}

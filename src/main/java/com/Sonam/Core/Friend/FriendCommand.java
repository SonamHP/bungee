package com.Sonam.Core.Friend;

import com.Sonam.Core.Core;
import com.Sonam.Core.Profiler.ProxyProfile;
import com.Sonam.Core.Utils.Cache;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class FriendCommand extends Command {

    public FriendCommand(String alias) {
        super(alias);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        ProxiedPlayer player = (ProxiedPlayer) sender;
        ProxyProfile profile = Core.getPpManager().getProfile(player.getUniqueId());
        if(args.length == 0) {
            player.sendMessage(new TextComponent("TODO"));
            return;
        }

        if(args[0].equalsIgnoreCase("add")) {
            if(Core.getPlayer(Cache.getCachedUUID(args[1].toLowerCase())) == null) {
                player.sendMessage(TextComponent.fromLegacyText(ChatColor.GOLD + "-----------------------------------------------------\n + " +
                        ChatColor.YELLOW + "You can't add that player because they're not online!\n" +
                        ChatColor.GOLD  + "-----------------------------------------------------"
                ));
                return;
            }

            return;
        }


    }
}

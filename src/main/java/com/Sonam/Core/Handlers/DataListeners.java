package com.Sonam.Core.Handlers;

import com.Sonam.Core.Core;
import com.Sonam.Core.Utils.Utils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.ArrayList;
import java.util.Map;

@SuppressWarnings("all")
public class DataListeners implements Listener {

    public Core plugin;
    Map<String, ServerInfo> servers = Core.getPlugin().getProxy().getServers();
    ArrayList<ServerInfo> serverInfos = new ArrayList<ServerInfo>();

    public DataListeners(Core plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onDataRecieved(DataRecievedEvent e) {
        System.out.println("DATA RECIEVED > " + e.getData());
        if(e.getData().equalsIgnoreCase("MCP_INIT")) {
            for(Map.Entry<String, ServerInfo> entry : servers.entrySet()) {
                serverInfos.add(entry.getValue());
            }

            for(ServerInfo serverInfo : serverInfos) {
                String data = "MCP reload";
                serverInfo.sendData("BungeeCord", data.getBytes());
            }

            Utils.sendStaffChat(ChatColor.translateAlternateColorCodes('&', "&4[SYSTEM] &fI am awake."));
        }
    }
}

package com.Sonam.Core.Handlers;

import com.Sonam.Core.Core;
import com.Sonam.Core.Profiler.ProfileApi;
import com.Sonam.Core.Profiler.ProxyProfile;
import com.Sonam.Core.Profiler.Rank;
import com.Sonam.Core.Utils.Cache;
import com.Sonam.Core.Utils.Utils;
import com.mongodb.client.MongoCollection;
import net.md_5.bungee.api.Callback;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.*;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import org.bson.Document;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * @author Sonam
 * Remake of pre-handlers using the new API.
 */
@SuppressWarnings("all")
public class LoginHandlers implements Listener {

    private Core plugin;

    HashSet<String> banned = new HashSet<String>();

    public LoginHandlers(Core plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPost(PostLoginEvent e) {
        MongoCollection<Document> profiles = Core.getMongo().getDatabaseNew().getCollection("profiles");

        ProfileApi api = new ProfileApi(e.getPlayer().getUniqueId());

        if(api.exists()) {
            ArrayList<Document> bans = (ArrayList<Document>) api.get("bans");
            ArrayList<Document> mutes = (ArrayList<Document>) api.get("mutes");

            if(!bans.isEmpty()) {
                Document ban = bans.get(bans.size() - 1);

                if(ban.getBoolean("active").equals(true)) {

                    DateTime banTime = new DateTime(ban.getString("end"));

                    if(ban.get("bantype").equals(1)) {
                        TextComponent ban_init = new TextComponent("You are permanently banned from this server. Reason: " + ban.getString("reason"));
                        ban_init.setColor(ChatColor.RED);

                        e.getPlayer().disconnect(ban_init);
                        return;
                    }
                    if(DateTime.now().isAfter(banTime)) {
                        ban.put("active", false);
                        ban.put("unbanReason", "Tempban Expired");
                        ban.put("end", DateTime.now().toString());
                        ban.put("unbannedBy", "Console");
                        bans.remove(bans.size() - 1);
                        bans.add(ban);
                        profiles.findOneAndUpdate(new Document("uniqueId", e.getPlayer().getUniqueId()),
                                new Document("$set", new Document("bans", bans)));
                        Core.getPpManager().createProfile(e.getPlayer().getUniqueId());
                        try {
                            Thread.sleep(300);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        } finally {
                            if(api.getRank().isStaff()) {
                                Core.getStaff().add(e.getPlayer().getUniqueId());
                                Utils.sendStaffChat(ChatColor.translateAlternateColorCodes('&', api.getPrefix() + " " + e.getPlayer().getName() + " &ejoined."));
                                ProxyProfile profile = Core.getPpManager().getProfile(e.getPlayer().getUniqueId());
                                ArrayList<String> staffOn = new ArrayList<String>();
                                if(staffOn.isEmpty()) {
                                    Core.getSocketIo().getSocket().emit("staffListing", staffOn);
                                }
                                for(UUID uuid : Core.getStaff()) {
                                    staffOn.add(ChatColor.translateAlternateColorCodes('&', Cache.getCachedPrefix(uuid) + " " + Cache.getCachedName(uuid) + "     &f" + e.getPlayer().getServer().getInfo().getName()));
                                }
                            }
                        }
                        return;
                    }
                    banned.add(e.getPlayer().getUniqueId().toString());
                    e.getPlayer().getPendingConnection().disconnect(new TextComponent(ChatColor.RED + "You are banned from this server.\n Reason: " + ban.get("reason") + ".\n Your suspension will end in: " + Days.daysBetween(DateTime.now(), banTime).getDays() + "D " + Hours.hoursBetween(DateTime.now(), banTime).getHours() + "H " + Minutes.minutesBetween(DateTime.now(), banTime).getMinutes() + "M"));
                    return;
                }
            }

            if(!mutes.isEmpty()) {
                Document mute = mutes.get(mutes.size() - 1);

                if(mute.getBoolean("active").equals(true)) {
                    DateTime muteTime = new DateTime((mute.get("end")));

                    if(DateTime.now().isAfter(muteTime)) {
                        mute.put("active", false);
                        mutes.remove(mutes.size() - 1);
                        mutes.add(mute);
                        profiles.findOneAndUpdate(new Document("uniqueId", e.getPlayer().getUniqueId()),
                                new Document("$set", new Document("mutes", mutes)));
                    }
                    try {
                        Thread.sleep(150);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }

            Core.getPpManager().createProfile(e.getPlayer().getUniqueId());

            ProxyProfile profile = Core.getPpManager().getProfile(e.getPlayer().getUniqueId());

            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }

            if(api.getRank().isStaff()) {
                Core.getStaff().add(e.getPlayer().getUniqueId());
                Utils.sendStaffChat(ChatColor.translateAlternateColorCodes('&', profile.getPrefix().replace("_", " ") + " " + profile.getUsername() + " &ejoined."));
                Utils.refreshStaffListing();
                return;
            }

            if(api.getRank().equals(Rank.YOUTUBER)) {
                Utils.sendStaffChat(ChatColor.translateAlternateColorCodes('&', profile.getPrefix().replace("_", " ") + " " + profile.getUsername() + " &ejoined."));
                return;
            }
            return;
        }

        MongoCollection<Document> cache = Core.getMongo().getDatabaseNew().getCollection("cache");
        MongoCollection<Document> friends = Core.getMongo().getDatabaseNew().getCollection("friends");

        profiles.insertOne(
                new Document("uniqueId", e.getPlayer().getUniqueId())
                .append("username", e.getPlayer().getName())
                .append("rank", "NORMAL")
                .append("package_rank", "NORMAL")
                .append("prefix", null)
                .append("level", 0)
                .append("options", new Document("chat", true).append("players", true))
                .append("gems", 10000)
                .append("bans", new ArrayList<Document>())
                .append("mutes", new ArrayList<Document>())
                .append("packages", new ArrayList<String>())
                .append("guild", null)
        );

        cache.insertOne(
                new Document("name", e.getPlayer().getName())
                .append("finder", e.getPlayer().getName().toLowerCase())
                .append("uniqueId", e.getPlayer().getUniqueId())
                .append("prefix", "&7")
        );

        Core.getCache().add(
                new Document("name", e.getPlayer().getName())
                .append("finder", e.getPlayer().getName().toLowerCase())
                .append("uniqueId", e.getPlayer().getUniqueId())
                .append("prefix", "&7")
        );

        friends.insertOne(
                new Document("uniqueId", e.getPlayer().getUniqueId())
                .append("friends", new ArrayList<UUID>())
        );

        Core.getPpManager().createProfile(e.getPlayer().getUniqueId());

        String sendProfile = "Profile " + e.getPlayer().getName() + " " + e.getPlayer().getUniqueId().toString() + " NORMAL NORMAL &7 null";
        e.getPlayer().getServer().sendData("BungeeCord", sendProfile.getBytes());
    }

    @EventHandler
    public void onConnect(ServerConnectEvent e) {
        ProxyProfile profile = Core.getPpManager().getProfile(e.getPlayer().getUniqueId());

        if(profile == null) {
            return;
        }

        String guildTag;
        if (Core.getGuildManager().getGuild(profile.getGuild()) != null) {
            guildTag = Core.getGuildManager().getGuild(profile.getGuild()).getTag();
        } else {
            guildTag = null;
        }

        String sendProfile = "Profile " + e.getPlayer().getName() + " " + e.getPlayer().getUniqueId().toString() + " " + profile.getRank().name() + " " + profile.getPackageRank().name() + " " + profile.getPrefix() + " " + guildTag;
        e.getTarget().sendData("BungeeCord", sendProfile.getBytes());
    }
}

package com.Sonam.Core.Handlers;

import com.Sonam.Core.Core;
import io.socket.client.IO;
import io.socket.client.Socket;
import net.md_5.bungee.api.ChatColor;

public class MasterWS {

    private Socket socket;

    public MasterWS(String websocketURL) {
        try {
            socket = IO.socket(websocketURL);
            Core.getPlugin().getProxy().getLogger().info(ChatColor.RED + "MasterControl Web socket connected.");
        } catch (Exception e) {
            e.printStackTrace();
            Core.getPlugin().getProxy().getLogger().info("SOCKET IO FAILED TO STARTUP");
            Core.getPlugin().getProxy().stop("Error initializing socket.io");
        }
    }

    public Socket getSocket() {
        return socket;
    }

}

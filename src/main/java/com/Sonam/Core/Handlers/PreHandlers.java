package com.Sonam.Core.Handlers;

import com.Sonam.Core.Core;
import com.Sonam.Core.Profiler.ProxyProfile;
import com.Sonam.Core.Profiler.Rank;
import com.Sonam.Core.Utils.Cache;
import com.Sonam.Core.Utils.Utils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.ArrayList;
import java.util.UUID;

public class PreHandlers implements Listener {

    private Core plugin;

    public PreHandlers(Core plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPreLogin(PreLoginEvent e) {

    }


//    @EventHandler
//    public void onLogin(PostLoginEvent e) {
//        DBCollection profiles = Core.getMongo().getDatabase().getCollection("profiles");
//
//        BasicDBObject query = new BasicDBObject();
//        query.put("uniqueId", e.getPlayer().getUniqueId());
//        DBCursor cursor = profiles.find(query);
//
//        if(cursor.hasNext()) {
//            DBObject result = cursor.next();
//            BasicDBList bans = (BasicDBList) result.get("bans");
//            BasicDBList mutes = (BasicDBList) result.get("mutes");
//
//            if(!bans.isEmpty()) {
//                BasicDBObject ban = (BasicDBObject) bans.get(bans.size() - 1);
//
//                if(ban.get("active").equals(true)) {
//                    DateTime banTime = new DateTime(ban.get("end"));
//
//                    if(ban.get("bantype").equals(1)) {
//                        e.getPlayer().getPendingConnection().disconnect(new TextComponent(ChatColor.RED + "You are banned from this server. Reason: " + ban.get("reason")));
//                        return;
//                    }
//                    if(DateTime.now().isAfter(banTime)) {
//                        P_API api = new P_API(e.getPlayer().getUniqueId());
//                        Core.getPpManager().createProfile(e.getPlayer().getUniqueId());
//                        ban.remove("active");
//                        ban.put("active", false);
//                        ban.remove("unbanreason");
//                        ban.remove("unbannedBy");
//                        ban.remove("end");
//                        ban.put("unbanreason", "Tempban Expired");
//                        ban.put("end", DateTime.now().toString());
//                        ban.put("unbannedBy", "Console");
//                        bans.remove(bans.size() - 1);
//                        bans.add(ban);
//                        profiles.update(query, new BasicDBObject("$set", new BasicDBObject("bans", bans)));
//                        try {
//                            Thread.sleep(1000);
//                        } catch (Exception ex) {
//                            ex.printStackTrace();
//                        } finally {
//                            if(api.getRank().isStaff()) {
//                                Core.getStaff().add(e.getPlayer().getUniqueId());
//                                Utils.sendStaffChat(ChatColor.translateAlternateColorCodes('&', api.getPrefix() + " " + e.getPlayer().getName() + " &ejoined."));
//                            }
//                        }
//                        return;
//                    }
//                    e.getPlayer().getPendingConnection().disconnect(new TextComponent(ChatColor.RED + "You are banned from this server. Reason: " + ban.get("reason") + ". Your suspension will end in: " + Days.daysBetween(DateTime.now(), banTime).getDays() + "D " + Hours.hoursBetween(DateTime.now(), banTime).getHours() + "H " + Minutes.minutesBetween(DateTime.now(), banTime).getMinutes() + "M"));
//                    return;
//                }
//            }
//
//            if(!mutes.isEmpty()) {
//                BasicDBObject mute = (BasicDBObject) mutes.get(mutes.size() - 1);
//
//                if(mute.get("active").equals(true)) {
//                    DateTime muteTime = new DateTime((mute.get("end")));
//
//                    if(DateTime.now().isAfter(muteTime)) {
//                        mute.remove("active");
//                        mute.put("active", false);
//                        mutes.remove(mutes.size() - 1);
//                        mutes.add(mute);
//                    }
//                }
//
//                profiles.update(query, new BasicDBObject("$set", new BasicDBObject("mutes", mutes)));
//
//            }
//
//            try {
//                Thread.sleep(300);
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
//
//            Core.getPpManager().createProfile(e.getPlayer().getUniqueId());
//
//            P_API api = new P_API(e.getPlayer().getUniqueId());
//
//            try {
//                Thread.sleep(100);
//            } catch (InterruptedException ex) {
//                ex.printStackTrace();
//            }
//
//            ProxyProfile profile = Core.getPpManager().getProfile(e.getPlayer().getUniqueId());
//
//            if(api.getRank().isStaff()) {
//                Core.getStaff().add(e.getPlayer().getUniqueId());
//                Utils.sendStaffChat(ChatColor.translateAlternateColorCodes('&', profile.getPrefix() + " " + profile.getUsername() + " &ejoined."));
//                return;
//            }
//
//            if(api.getRank().equals(Rank.YOUTUBER)) {
//                Utils.sendStaffChat(ChatColor.translateAlternateColorCodes('&', profile.getPrefix() + " " + profile.getUsername() + " &ejoined."));
//                return;
//            }
//
//            return;
//        }
//
//        DBCollection cache = Core.getMongo().getDatabase().getCollection("cache");
//        DBCollection friends = Core.getMongo().getDatabase().getCollection("friends");
//
//        BasicDBObject newProfile = new BasicDBObject();
//        newProfile.put("uniqueId", e.getPlayer().getUniqueId());
//        newProfile.put("username", e.getPlayer().getName());
//        newProfile.put("rank", "NORMAL");
//        newProfile.put("package_rank", "NORMAL");
//        newProfile.put("prefix", null);
//        newProfile.put("level", 0);
//        newProfile.put("options", new BasicDBObject().append("chat", true).append("players", true));
//        newProfile.put("gems", 10000);
//        newProfile.put("banned", false);
//        newProfile.put("bans", new BasicDBList());
//        newProfile.put("muted", false);
//        newProfile.put("mutes", new BasicDBList());
//        newProfile.put("packages", new BasicDBList());
//        newProfile.put("guild", null);
//
//        BasicDBObject newCache = new BasicDBObject();
//        newCache.put("name", e.getPlayer().getName());
//        newCache.put("finder", e.getPlayer().getName().toLowerCase());
//        newCache.put("uniqueId", e.getPlayer().getUniqueId());
//        newCache.put("prefix", "&7");
//
//        BasicDBObject newPF = new BasicDBObject();
//        newPF.put("uniqueId", e.getPlayer().getUniqueId());
//        newPF.put("friends", new BasicDBList());
//
//        profiles.insert(newProfile);
//        cache.insert(newCache);
//        friends.insert(newPF);
//
//        Core.getCache().add(newCache);
//
//        plugin.getProxy().getLogger().info("Profile Created > " + e.getPlayer().getName());
//
//        Core.getPpManager().createProfile(e.getPlayer().getUniqueId());
//    }


    @EventHandler
    public void onDisconnect(PlayerDisconnectEvent e) {
        ProxyProfile profile = Core.getPpManager().getProfile(e.getPlayer().getUniqueId());

        if(profile == null) {
            return;
        }

        if(profile.getRank().equals(Rank.YOUTUBER)) {
            Utils.sendStaffChat(ChatColor.translateAlternateColorCodes('&', profile.getPrefix().replace("_", " ") + " " + profile.getUsername() + " &edisconnected."));
        }

        if(Core.getStaff().contains(e.getPlayer().getUniqueId())) {
            Core.getStaff().remove(e.getPlayer().getUniqueId());
            Utils.sendStaffChat(ChatColor.translateAlternateColorCodes('&', profile.getPrefix().replace("_", " ") + " " + profile.getUsername() + " &edisconnected."));
        }

        if(Core.getPpManager().getProfile(e.getPlayer().getUniqueId()) != null) {
            Core.getPpManager().unloadProfile(e.getPlayer().getUniqueId());
        }

        Utils.refreshStaffListing();
    }

    @EventHandler
    public void onChat(ChatEvent e) {
        char charAt = e.getMessage().charAt(0);
        if(charAt == '/') {
            return;
        }
        ProxiedPlayer player = (ProxiedPlayer) e.getSender();
        ProxyProfile profile = Core.getPpManager().getProfile(player.getUniqueId());
        if(profile.isMuted()) {
            player.sendMessage(new TextComponent(ChatColor.RED + "You are currently muted."));
            e.setMessage("");
            e.setCancelled(true);
        }
    }

}

package com.Sonam.Core.Handlers;

import net.md_5.bungee.api.plugin.Event;

public class DataRecievedEvent extends Event {
    private final String data;

    public DataRecievedEvent(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

}

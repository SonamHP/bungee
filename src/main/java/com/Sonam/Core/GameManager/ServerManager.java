package com.Sonam.Core.GameManager;

import com.Sonam.Core.Core;
import com.mongodb.client.MongoCollection;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public class ServerManager {

    private MongoCollection<Document> collection;
    private List<Document> results;
    private Document result;
    public ServerManager() {
    }

    public String getInstanceType(String instanceID) {
        collection = Core.getMongo().getDatabaseNew().getCollection("serverManager");
        results = collection.find(new Document("_id", instanceID)).into(new ArrayList<Document>());
        result = results.get(0);

        return result.getString("type");
    }

}

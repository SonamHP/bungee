package com.Sonam.Core.Utils;

import com.Sonam.Core.Core;
import com.Sonam.Core.Profiler.ProxyProfile;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.UUID;

public class Utils {

    public static void sendStaffChat(String message) {
        DateTime dt = new DateTime();
        Core.getSocketIo().getSocket().open();
        if(dt.getMinuteOfHour() < 10) {
            Core.getSocketIo().getSocket().emit("staffChat", ChatColor.GRAY.toString() + dt.getHourOfDay() + ":0" + dt.getMinuteOfHour() + " " + message);
        } else {
            Core.getSocketIo().getSocket().emit("staffChat", ChatColor.GRAY.toString() + dt.getHourOfDay() + ":" + dt.getMinuteOfHour() + " " + message);
        }


        for(UUID uuid : Core.getStaff()) {
            BungeeCord.getInstance().getPlayer(uuid).sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "&b[STAFF] &f" + message)));
        }
    }

    public static void refreshStaffListing() {
        ArrayList<String> staffOn = new ArrayList<String>();
        for(UUID uuid : Core.getStaff()) {
            ProxyProfile profile = Core.getPpManager().getProfile(uuid);
            staffOn.add(profile.getRank().getLevel() + ChatColor.translateAlternateColorCodes('&', Cache.getCachedPrefix(uuid) + " " + Cache.getCachedName(uuid)));
        }

        Comparator comparator = Collections.reverseOrder();
        Collections.sort(staffOn, comparator);
        Core.getSocketIo().getSocket().emit("staffListing", staffOn);
        System.out.println(staffOn);
    }

    public static String FRIEND_NO_ARGS = "\n"
            + "\n"
            + "\n"
            + "\n"
            + "";

}

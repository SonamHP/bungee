package com.Sonam.Core.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.UUID;

public class UUIDFetcher {

    String username;

    public UUIDFetcher(String username) {
        this.username = username;
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            return new JSONObject(jsonText);
        } finally {
            is.close();
        }
    }

    public UUID fetchUuid() {
        if(username != null) {
            try {
                JSONObject json = readJsonFromUrl("https://us.mc-api.net/v3/uuid/" + username);

                return UUID.fromString((String) json.get("full_uuid"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return null;
    }

    public String fetchCorrectName() {
        if(username != null) {
            try {
                JSONObject json = readJsonFromUrl("https://us.mc-api.net/v3/uuid/" + username);

                return (String) json.get("name");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return null;
    }

}

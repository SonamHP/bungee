package com.Sonam.Core.Utils;

import com.Sonam.Core.Core;
import com.mongodb.DBObject;
import org.bson.Document;

import java.util.UUID;

public class Cache {

    public static String getCachedName(UUID uuid) {
        for(Document document : Core.getCache()) {
            if(document.get("uniqueId").equals(uuid)) {
                return (String) document.get("name");
            }
        }
        return null;
    }

    public static Document getCachedPlayer(UUID uuid) {
        for(Document document : Core.getCache()) {
            if(document.get("uniqueId").equals(uuid)) {
                return document;
            }
        }
        return null;
    }

    public static String getCachedPrefix(UUID uuid) {
        for(Document document : Core.getCache()) {
            if(document.get("uniqueId").equals(uuid)) {
                return (String) document.get("prefix");
            }
        }
        return null;
    }

    public static UUID getCachedUUID(String name) {
        for(Document document : Core.getCache()) {
            if(document.get("finder").equals(name)) {
                return (UUID) document.get("uniqueId");
            }
        }
        return null;
    }

}

package com.Sonam.Core;

import com.Sonam.Core.Commands.*;
import com.Sonam.Core.Friend.FriendCommand;
import com.Sonam.Core.Guild.GuildLoader;
import com.Sonam.Core.Guild.GuildManager;
import com.Sonam.Core.Handlers.*;
import com.Sonam.Core.GameManager.ServerManager;
import com.Sonam.Core.Profiler.*;
import com.Sonam.Core.Profiler.Package;
import com.Sonam.Core.SocketListeners.Punishment;
import com.Sonam.Core.Threads.CacheLoader;
import com.Sonam.Core.Threads.DataReciever;
import com.Sonam.Core.Utils.Cache;
import com.Sonam.Core.Utils.Utils;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import org.bson.Document;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class Core extends Plugin {

    private static Core plugin;
    private static MongoDB mongoDB;
    private static Set<Document> cache;
    private static Set<UUID> staff;
    private static PPManager ppManager = null;
    private static Set<String> ranks = new HashSet<String>();
    private static Set<String> packageRanks = new HashSet<String>();
    private static GuildManager guildManager = null;
    private static HashSet<String> disbandRequests = new HashSet<String>();
    private static HashMap<String, String> friendRequests = new HashMap<String, String>();
    private static Set<String> packages = new HashSet<String>();
    private static HashMap<String, String> invites;
    private static ServerManager serverManager;
    private static Socket_IO socketIo;
    private static MasterWS masterWS;
    private static Cache cacheManager;

    public void onEnable() {
        plugin = this;
        cache = new HashSet<Document>();
        ppManager = new PPManager(this);
        cacheManager = new Cache();
        guildManager = new GuildManager();
        serverManager = new ServerManager();
        invites = new HashMap<String, String>();

        MongoDB mdb = new MongoDB();
        mdb.setDatabase("main_server");
        mdb.setDatabaseNew("main_server");
        mongoDB = mdb;

        for(Rank rank : Rank.values()) {
            ranks.add(rank.name());
        }

        for(PackageRank packageRank : PackageRank.values()) {
            packageRanks.add(packageRank.name());
        }

        for(Package pack : Package.values()) {
            packages.add(pack.name());
        }

        socketIo = new Socket_IO("http://localhost:1420");
        masterWS = new MasterWS("http://localhost:3000");

        staff = new HashSet<UUID>();

        getProxy().getPluginManager().registerListener(this, new PreHandlers(this));
        getProxy().getPluginManager().registerListener(this, new LoginHandlers(this));
        getProxy().getPluginManager().registerListener(this, new DataListeners(this));
        getProxy().getPluginManager().registerCommand(this, new Userinfo());
        getProxy().getPluginManager().registerCommand(this, new StaffChat());
        getProxy().getPluginManager().registerCommand(this, new SetRank());
        getProxy().getPluginManager().registerCommand(this, new PackageRankCommand());
        getProxy().getPluginManager().registerCommand(this, new FriendCommand("f"));
        getProxy().getPluginManager().registerCommand(this, new GuildCommand("g"));
        getProxy().getPluginManager().registerCommand(this, new GuildCommand("guild"));
        getProxy().getPluginManager().registerCommand(this, new GuildCommand("gchat"));
        getProxy().getPluginManager().registerCommand(this, new PackageCommand("package"));
        getProxy().getPluginManager().registerCommand(this, new PackageCommand("pkg"));
        getProxy().getPluginManager().registerCommand(this, new GoliathCommand());
        getProxy().getPluginManager().registerCommand(this, new Mongo_New_Test());
        getProxy().getPluginManager().registerCommand(this, new Ban());
        getProxy().getPluginManager().registerCommand(this, new TempBan());
        getProxy().getPluginManager().registerCommand(this, new Unban());
        getProxy().getPluginManager().registerCommand(this, new Bans());
        getProxy().getPluginManager().registerCommand(this, new Mute());
        getProxy().getPluginManager().registerCommand(this, new Unmute());
        getProxy().getPluginManager().registerCommand(this, new TpTo());
        getProxy().getPluginManager().registerCommand(this, new MCP());

        getProxy().getScheduler().runAsync(this, new CacheLoader());
        getProxy().getScheduler().runAsync(this, new GuildLoader());

        final Socket socket = getSocketIo().getSocket();
        final Socket master = getMaster().getSocket();

        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            public void call(Object... objects) {
//                socket.emit("staffChat", ChatColor.translateAlternateColorCodes('&', "&c[MASTER] &fAttached"));
            }
        });

        socket.on("from_daemon", new Emitter.Listener() {
            public void call(Object... args) {
                JSONObject message = (JSONObject) args[0];
                try {
                    UUID uuid = Cache.getCachedUUID(message.getString("username").toLowerCase());
                    Utils.sendStaffChat(ChatColor.translateAlternateColorCodes('&', "&d[WEB] " + Cache.getCachedPrefix(uuid) + " " + message.getString("username") + "&f: " + message.getString("message")));
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        socket.on("slack_med", new Emitter.Listener() {
            public void call(Object... args) {
                String[] start = args[0].toString().split(" ");
                String username = start[0];
                StringBuilder builder = new StringBuilder();
                for(int i = 1; i < start.length; i++) {
                    builder.append(start[i]).append(" ");
                }
                String message = builder.toString().trim();
                Utils.sendStaffChat(ChatColor.translateAlternateColorCodes('&', "&e[SLACK] &c[ADMIN] " + username + "&f: " + message));
            }
        });

        socket.on("socket_connected", new Emitter.Listener() {
            public void call(Object... objects) {
                System.out.println("Reloading Sockets");
                Utils.refreshStaffListing();
            }
        });

        socket.on("punish", new Punishment());

        socket.on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
            public void call(Object... objects) {

            }
        });

        master.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            public void call(Object... objects) {

            }
        });

        master.on("data", new DataReciever());

        master.on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
            public void call(Object... objects) {

            }
        });

        master.connect();
        socket.connect();

    }

    public void onDisable() {
        socketIo.getSocket().close();
        masterWS.getSocket().close();
    }

    public static Cache getCacheManager() {
        return cacheManager;
    }

    public static ServerManager getServerManager() {
        return serverManager;
    }

    public static GuildManager getGuildManager() {
        return guildManager;
    }

    public static Core getPlugin() {
        return plugin;
    }

    public static MongoDB getMongo() {
        return mongoDB;
    }

    public static Set<Document> getCache() {
        return cache;
    }

    public static HashMap<String, String> getInvites() {
        return invites;
    }

    public static Set<UUID> getStaff() {
        return staff;
    }

    public static PPManager getPpManager() {
        return ppManager;
    }

    public static Set<String> getRanks() {
        return ranks;
    }

    public static Set<String> getPackageRanks() {
        return packageRanks;
    }

    public static HashSet<String> getDisbandRequests() {
        return disbandRequests;
    }

    public static HashMap<String, String> getFriendRequests() {
        return friendRequests;
    }

    public static Socket_IO getSocketIo() {
        return socketIo;
    }

    public static Set<String> getPackages() {
        return packages;
    }

    public static MasterWS getMaster() {
        return masterWS;
    }

    public static ProxiedPlayer getPlayer(UUID uuid) {
        return getPlugin().getProxy().getPlayer(uuid);
    }

}

package com.Sonam.Core.Threads;

import com.Sonam.Core.Core;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.client.MongoCollection;
import org.bson.Document;

import java.util.ArrayList;

import static net.md_5.bungee.api.ChatColor.translateAlternateColorCodes;

public class CacheLoader implements Runnable {

    MongoCollection<Document> cache = Core.getMongo().getDatabaseNew().getCollection("cache");

    public void run() {

        ArrayList<Document> cursor = cache.find().into(new ArrayList<Document>());

        if(cursor.isEmpty()) {
            Core.getPlugin().getProxy().getLogger().info(translateAlternateColorCodes('&', "&a[CORE] &fNo cache found!"));
            return;
        }

        if(!Core.getCache().isEmpty()) {
            Core.getCache().clear();
        }

        for(Document document : cursor) {
            Core.getCache().add(document);
        }

        try {
            Thread.sleep(5);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

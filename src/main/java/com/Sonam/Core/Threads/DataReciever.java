package com.Sonam.Core.Threads;

import com.Sonam.Core.Core;
import com.Sonam.Core.Handlers.DataRecievedEvent;
import io.socket.emitter.Emitter;

public class DataReciever implements Emitter.Listener {

    public void call(Object... args) {
        Core.getPlugin().getProxy().getPluginManager().callEvent(new DataRecievedEvent(args[0].toString()));
    }

}

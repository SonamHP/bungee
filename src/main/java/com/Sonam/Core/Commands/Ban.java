package com.Sonam.Core.Commands;

import com.Sonam.Core.Core;
import com.Sonam.Core.Profiler.ProfileApi;
import com.Sonam.Core.Profiler.ProxyProfile;
import com.Sonam.Core.Utils.Cache;
import com.Sonam.Core.Utils.Utils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.bson.Document;
import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.UUID;

import static net.md_5.bungee.api.ChatColor.*;

public class Ban extends Command {

    public Ban() {
        super("ban");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        ProxiedPlayer player = (ProxiedPlayer) sender;
        ProxyProfile profile = Core.getPpManager().getProfile(player.getUniqueId());

        if(profile.getRank().getLevel() >= 3) {

            if(args.length < 2) {
                player.sendMessage(new TextComponent(RED + "/ban <name> <reason>"));
                return;
            }

            ProfileApi api = new ProfileApi(Cache.getCachedUUID(args[0].toLowerCase()));

            if(!api.exists()) {
                player.sendMessage(new TextComponent(RED + "That player does not exist!"));
                return;
            }

            ArrayList<Document> bans = (ArrayList<Document>) api.get("bans");

            if(!bans.isEmpty()) {
                if(bans.get(bans.size() - 1).get("active").equals(true)) {
                    player.sendMessage(new TextComponent(ChatColor.RED + "That player is already banned."));
                    return;
                }
            }

            try {
                Thread.sleep(300);
            } catch (Exception e) {
                e.printStackTrace();
            }



            UUID targetUUID = Cache.getCachedUUID(args[0].toLowerCase());

            StringBuilder builder = new StringBuilder();
            for(int i = 1; i < args.length; i++) {
                builder.append(args[i]).append(" ");
            }
            String reason = builder.toString().trim();

            bans.add(new Document("bantype", 1).append("bannedBy", player.getUniqueId())
                    .append("reason", reason).append("active", true).append("date", new LocalDate().toString())
            );

            Core.getMongo().getDatabaseNew().getCollection("profiles")
                    .findOneAndUpdate(new Document("uniqueId", targetUUID),
                            new Document("$set", new Document("bans", bans)));


            if(Core.getPlayer(targetUUID) != null) {

                Core.getPlayer(targetUUID).disconnect(new TextComponent(RED + "You have been permanently banned from the server for " + reason + "."));
                Utils.sendStaffChat(WHITE + player.getName() + " kicked player \'" + Cache.getCachedName(targetUUID) + "\'. Reason: " + reason + ".");
            }
            player.sendMessage(new TextComponent(YELLOW + "Banned player \'" + Cache.getCachedName(targetUUID) + "\'"));
            Utils.sendStaffChat(WHITE + player.getName() + " banned player \'" + Cache.getCachedName(targetUUID) + "\'. Reason: " + reason + ".");


            return;
        }
        player.sendMessage(new TextComponent(RED + "You do not have permission to do this."));

    }
}

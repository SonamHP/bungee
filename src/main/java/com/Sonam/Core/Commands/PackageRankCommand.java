package com.Sonam.Core.Commands;

import com.Sonam.Core.Core;
import com.Sonam.Core.Profiler.P_API;
import com.Sonam.Core.Profiler.PackageRank;
import com.Sonam.Core.Profiler.ProxyProfile;
import com.Sonam.Core.Profiler.Rank;
import com.Sonam.Core.Utils.Cache;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

public class PackageRankCommand extends Command {

    public PackageRankCommand() {
        super("packagerank");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        DBCollection collection = Core.getMongo().getDatabase().getCollection("profiles");

        ProxiedPlayer player = (ProxiedPlayer) sender;

        ProxyProfile profile = Core.getPpManager().getProfile(player.getUniqueId());

        if(!profile.getRank().equals(Rank.ADMIN)) {
            player.sendMessage(new TextComponent(ChatColor.RED + "You do not have permission to do this."));
            return;
        }

        if(args.length < 2) {
            player.sendMessage(new TextComponent(ChatColor.RED + "/packagerank <player> <package_rank> [purchase]"));
            return;
        }

        if(!Core.getPackageRanks().contains(args[1].toUpperCase())) {
            player.sendMessage(new TextComponent(ChatColor.RED + "Invalid package rank!"));
            return;
        }

        UUID uuid = Cache.getCachedUUID(args[0].toLowerCase());

        BasicDBObject query = new BasicDBObject("uniqueId", uuid);
        DBCursor cursor = collection.find(query);

        if(cursor.hasNext()) {
            P_API api = new P_API(uuid);

            for(PackageRank packageRank : PackageRank.values()) {
                if(packageRank.name().equalsIgnoreCase(args[1].toUpperCase())) {

                    BasicDBObject update = new BasicDBObject();
                    update.put("package_rank", args[1].toUpperCase());

                    collection.update(query, new BasicDBObject("$set", update));

                    if(api.getRank().equals(Rank.NORMAL)) {
                        api.updateCachePrefix(packageRank.getPrefix());
                    }

                    if(Core.getPlugin().getProxy().getPlayer(uuid) != null) {
                        ProxyProfile target = Core.getPpManager().getProfile(uuid);
                        target.setPackageRank(packageRank);
                    }

                    if(args.length == 3) {
                        if(args[2].equalsIgnoreCase("purchase")) {
                            if(Core.getPlugin().getProxy().getPlayer(uuid) != null) {
                                TextComponent message = new TextComponent(ChatColor.translateAlternateColorCodes('&', "&3[STORE] &aYour " + packageRank.name() + " package has arrived. Relog for it to take &afull &aeffect! &aThanks for your purchase!"));
                                message.setColor(ChatColor.GRAY);
                                Core.getPlugin().getProxy().getPlayer(uuid).sendMessage(message);
                            }
                            return;
                        }
                        return;
                    }

                    player.sendMessage(new TextComponent(ChatColor.GREEN + "Set " + args[0] + "\'s PackageRank to " + packageRank.name()));



                }
            }


            return;
        }
        player.sendMessage(new TextComponent(ChatColor.RED + "Player not found."));

    }
}

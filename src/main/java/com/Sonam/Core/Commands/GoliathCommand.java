package com.Sonam.Core.Commands;

import com.Sonam.Core.Core;
import com.Sonam.Core.Goliath.PasswordGenerator;
import com.Sonam.Core.Profiler.ProxyProfile;
import com.mongodb.client.MongoCollection;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.bson.Document;

import java.util.ArrayList;

public class GoliathCommand extends Command {

    public GoliathCommand() {
        super("goliath");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        MongoCollection<Document> collection = Core.getMongo().getDatabaseNew().getCollection("goliath");
        PasswordGenerator generator = new PasswordGenerator();
        ProxiedPlayer player = (ProxiedPlayer) sender;
        ProxyProfile profile = Core.getPpManager().getProfile(player.getUniqueId());

        if(profile.getRank().getLevel() > 2) {
            ArrayList<Document> results = collection.find(new Document("uniqueId", player.getUniqueId().toString())).into(new ArrayList<Document>());
            if(results.isEmpty()) {
                final String password = generator.nextSessionId();
                TextComponent clickable = new TextComponent("http://http://40.77.64.80:1420/?new=Sonam");
                clickable.setColor(ChatColor.GREEN);
                clickable.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Click to get your goliath password!").create()));
                clickable.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "http://40.77.64.80:1420/?new=" + player.getName()));
                collection.insertOne(new Document("uniqueId", player.getUniqueId().toString()).append("username", player.getName()).append("password", password).append("new", true));
                player.sendMessage(clickable);
                return;
            }
            player.sendMessage(TextComponent.fromLegacyText("You already have a goliath account!"));
            return;
        }
        player.sendMessage(TextComponent.fromLegacyText("You do not permission to do this."));

    }
}

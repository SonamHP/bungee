package com.Sonam.Core.Commands;

import com.Sonam.Core.Core;
import com.Sonam.Core.Profiler.ProfileApi;
import com.Sonam.Core.Profiler.ProxyProfile;
import com.Sonam.Core.Utils.Cache;
import com.Sonam.Core.Utils.Utils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.bson.Document;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.UUID;

import static net.md_5.bungee.api.ChatColor.RED;

public class Mute extends Command {

    public Mute() {
        super("mute");
    }

    public void execute(CommandSender sender, String[] args) {

        ProxiedPlayer player = (ProxiedPlayer) sender;
        ProxyProfile profile = Core.getPpManager().getProfile(player.getUniqueId());

        if(profile.getRank().getLevel() >= 2) {

            if(args.length < 3) {
                player.sendMessage(new TextComponent(RED + "/mute <player> <time> <reason>"));
                return;
            }

            ProfileApi api = new ProfileApi(Cache.getCachedUUID(args[0].toLowerCase()));

            if(Cache.getCachedUUID(args[0].toLowerCase()) == null) {
                player.sendMessage(new TextComponent(RED + "That player does not exist!"));
                return;
            }

            if(!api.exists()) {
                player.sendMessage(new TextComponent(RED + "That player does not exist!"));
                return;
            }

            ArrayList<Document> mutes = (ArrayList<Document>) api.get("mutes");

            if(!mutes.isEmpty()) {
                if(mutes.get(mutes.size() - 1).get("active").equals(true)) {
                    player.sendMessage(new TextComponent(ChatColor.RED + "That person is already muted"));
                    return;
                }
            }

            try {
                Thread.sleep(300);
            } catch (Exception e) {
                e.printStackTrace();
            }



            UUID targetUUID = Cache.getCachedUUID(args[0].toLowerCase());

            StringBuilder builder = new StringBuilder();
            for(int i = 2; i < args.length; i++) {
                builder.append(args[i]).append(" ");
            }
            String reason = builder.toString().trim();

            Document document = new Document("mutedBy", player.getUniqueId()).append("reason", reason)
                    .append("start", DateTime.now().toString()).append("active", true);

            String initTime = args[1];

            char[] array = initTime.toCharArray();
            char timespam = array[array.length - 1];
            StringBuilder sb = new StringBuilder(initTime);
            sb.deleteCharAt(initTime.length() - 1);
            int finalTime = Integer.parseInt(sb.toString().trim());

            String brs = null;

            switch (timespam) {
                case 'm':
                    document.append("end", DateTime.now().plusMinutes(finalTime).toString());
                    brs = "0D:0H:" + finalTime + "M";
                    document.append("time_", brs);
                    break;
                case 'M':
                    document.append("end", DateTime.now().plusMinutes(finalTime).toString());
                    brs = "0D:0H:" + finalTime + "M";
                    document.append("time_", brs);
                    break;
                case 'h':
                    document.append("end", DateTime.now().plusHours(finalTime).toString());
                    brs = "0D:" + finalTime + "H:0M";
                    document.append("time_", brs);
                    break;
                case 'H':
                    document.append("end", DateTime.now().plusHours(finalTime).toString());
                    brs = "0D:" + finalTime + "H:0M";
                    document.append("time_", brs);
                    break;
                case 'd':
                    document.append("end", DateTime.now().plusDays(finalTime).toString());
                    brs = finalTime + "D:0H:0M";
                    document.append("time_", brs);
                    break;
                case 'D':
                    document.append("end", DateTime.now().plusDays(finalTime).toString());
                    brs = finalTime + "D:0H:0M";
                    document.append("time_", brs);
                    break;
                default:
                    player.sendMessage(new TextComponent(ChatColor.RED + "Please input a correct timestamp. Ex: 1H"));
                    return;
            }

            mutes.add(document);

            Core.getMongo().getDatabaseNew().getCollection("profiles").findOneAndUpdate(new Document("uniqueId", targetUUID),
                    new Document("$set", new Document("mutes", mutes)));

            Utils.sendStaffChat(ChatColor.WHITE + player.getName() + " muted " + Cache.getCachedName(targetUUID) + " for " + brs + " for " + reason + ".");

            if(Core.getPlayer(targetUUID) != null) {
                ProxyProfile profile2 = Core.getPpManager().getProfile(targetUUID);
                profile2.setMuted(true);
                TextComponent mutee = new TextComponent("You have been muted for " + brs + " for the reason " + reason + ".");
                mutee.setColor(ChatColor.RED);
                mutee.setBold(true);
                Core.getPlayer(targetUUID).sendMessage(mutee);
                return;
            }

            return;
        }
        player.sendMessage(new TextComponent(ChatColor.RED + "You are not allowed to do this."));

    }

}

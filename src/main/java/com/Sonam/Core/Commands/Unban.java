package com.Sonam.Core.Commands;

import com.Sonam.Core.Core;
import com.Sonam.Core.Profiler.ProfileApi;
import com.Sonam.Core.Profiler.ProxyProfile;
import com.Sonam.Core.Utils.Cache;
import com.Sonam.Core.Utils.Utils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.bson.Document;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.UUID;

import static net.md_5.bungee.api.ChatColor.WHITE;
import static net.md_5.bungee.api.ChatColor.YELLOW;

public class Unban extends Command {

    public Unban() {
        super("unban");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        ProxiedPlayer player = (ProxiedPlayer) sender;
        ProxyProfile profile = Core.getPpManager().getProfile(player.getUniqueId());

        if(profile.getRank().getLevel() >= 3) {


            if(args.length < 2) {
                player.sendMessage(new TextComponent(ChatColor.RED + "/unban <player> <reason>"));
                return;
            }

            UUID uuid = Cache.getCachedUUID(args[0].toLowerCase());
            ProfileApi api = new ProfileApi(uuid);

            if(uuid == null) {
                player.sendMessage(new TextComponent(ChatColor.RED + "No player found by that name!"));
                return;
            }

            StringBuilder builder = new StringBuilder();
            for(int i = 1; i < args.length; i++) {
                builder.append(args[i]).append(" ");
            }
            String reason = builder.toString().trim();

            ArrayList<Document> bans = (ArrayList<Document>) api.get("bans");

            if(bans.isEmpty()) {
                player.sendMessage(new TextComponent(ChatColor.RED + "That player is not banned."));
                return;
            }

            Document ban = bans.get(bans.size() - 1);

            if(ban.get("active").equals(false)) {
                player.sendMessage(new TextComponent(ChatColor.RED + "That player is not banned."));
                return;
            }

            bans.remove(bans.size() - 1);
            ban.remove("unbanReason");
            ban.remove("unbannedBy");
            ban.remove("active");
            if(!ban.get("bantype").equals(1)) {
                ban.remove("end");
                ban.put("end", DateTime.now().toString());
            }
            ban.put("unbanReason", reason);
            ban.put("active", false);
            ban.put("unbannedBy", player.getUniqueId());

            bans.add(ban);

            Core.getMongo().getDatabaseNew().getCollection("profiles").findOneAndUpdate(new Document("uniqueId", uuid), new Document("$set", new Document("bans", bans)));

            player.sendMessage(new TextComponent(YELLOW + "Unbanned \'" + Cache.getCachedName(uuid) + "\'"));
            Utils.sendStaffChat(WHITE + player.getName() + " unbanned player \'" + Cache.getCachedName(uuid) + "\'. Reason: " + reason);

            return;
        }
        player.sendMessage(new TextComponent(ChatColor.RED + "You do not have permission to use this command."));


    }
}

package com.Sonam.Core.Commands;

import com.Sonam.Core.Core;
import com.Sonam.Core.Profiler.ProxyProfile;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class TpTo extends Command {

    public TpTo() {
        super("tpto");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        ProxiedPlayer player = (ProxiedPlayer) sender;
        ProxyProfile profile = Core.getPpManager().getProfile(player.getUniqueId());

        if(profile.getRank().getLevel() >= 3) {

            if(args.length < 1) {
                player.sendMessage(new TextComponent(ChatColor.RED + "/tpto <player>"));
                return;
            }

            if(Core.getPlugin().getProxy().getPlayer(args[0]) != null) {
                ProxiedPlayer target = Core.getPlugin().getProxy().getPlayer(args[0]);

                player.connect(target.getServer().getInfo());
                player.sendMessage(new TextComponent(ChatColor.GREEN + "Sending you to " + target.getServer().getInfo().getName() + " with " + target.getName()));

                if(player.getServer().getInfo().equals(target.getServer().getInfo())) {
                    return;
                }

                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                String port = "Port " + sender.getName() + " " + target.getName();
                target.getServer().getInfo().sendData("BungeeCord", port.getBytes());
                return;
            }

            player.sendMessage(new TextComponent(ChatColor.RED + "That player is not online!"));

            return;
        }

        player.sendMessage(new TextComponent(ChatColor.RED + "You are not allowed to do this!"));

    }
}

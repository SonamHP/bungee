package com.Sonam.Core.Commands;

import com.Sonam.Core.Core;
import com.Sonam.Core.Guild.Guild;
import com.Sonam.Core.Profiler.ProfileApi;
import com.Sonam.Core.Profiler.ProxyProfile;
import com.Sonam.Core.Profiler.Rank;
import com.Sonam.Core.Utils.Cache;
import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.InsertOneOptions;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.bson.Document;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class GuildCommand extends Command {

    String alias;

    public GuildCommand(String alias) {
        super(alias);
        this.alias = alias;
    }

    public void execute(CommandSender sender, String[] args) {

        if(!(sender instanceof ProxiedPlayer)) {
            Core.getPlugin().getProxy().getLogger().info("Only players can do this");
            return;
        }

        ProxiedPlayer player = (ProxiedPlayer) sender;

        ProxyProfile profile = Core.getPpManager().getProfile(player.getUniqueId());

        DBCollection collection = Core.getMongo().getDatabase().getCollection("guilds");
        MongoCollection<Document> newCol = Core.getMongo().getDatabaseNew().getCollection("guilds");

        final UUID uuid = player.getUniqueId();

        ProfileApi api = new ProfileApi(uuid);

        if(alias.equalsIgnoreCase("gchat")) {
            if(profile.getGuild() == null) {
                player.sendMessage(new TextComponent(ChatColor.RED + "You must be in a guild to do that!"));
                return;
            }

            if(args.length < 1) {
                player.sendMessage(new TextComponent(ChatColor.RED + "/g chat <message>"));
                return;
            }

            StringBuilder builder = new StringBuilder();
            for(int i = 0; i < args.length; i++) {
                builder.append(args[i]).append(" ");
            }
            String message = builder.toString().trim();

            ArrayList<Document> members = Core.getGuildManager().getGuild(profile.getGuild()).getMembers();
            for(Document member : members) {
                UUID uuid_member = (UUID) member.get("uuid");
                if(Core.getPlayer(uuid_member) != null) {
                    Core.getPlayer(uuid_member).sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&',
                            "&2Guild > " + profile.getPrefix() + " " + player.getName() + "&f: " + message
                    )));
                }
            }
            return;
        }

        if(args.length == 0) {
            sender.sendMessage(new TextComponent(ChatColor.YELLOW + "TODO"));
            return;
        }

        if(args[0].equalsIgnoreCase("create")) {

            if(profile.getGuild() != null) {
                if(profile.getGuild().equals(player.getUniqueId())) {
                    player.sendMessage(new TextComponent(ChatColor.RED + "You already own a guild!"));
                    return;
                }
                player.sendMessage(new TextComponent(ChatColor.RED + "You are already in a guild! Leave it in order to make your own!"));
                return;
            }

            if(!(profile.getPackageRank().getLevel() > 1)) {
                player.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "&bYou must have at least &aVIP&6+ &bto create a guild! Purchase it at &estore.sweg.life")));
                return;
            }

            if(args.length < 2) {
                player.sendMessage(new TextComponent(ChatColor.RED + "/g create <name>"));
                return;
            }

            StringBuilder builder = new StringBuilder();
            for(int i = 1; i < args.length; i++) {
                builder.append(args[i]).append(" ");
            }
            String name = builder.toString().trim();

            BasicDBObject query = new BasicDBObject("name", name);
            DBCursor cursor = collection.find(query);

            if(name.length() < 4) {
                player.sendMessage(new TextComponent(ChatColor.RED + "Guilds must have at least 4 characters on their name!"));
            }

            if(cursor.hasNext()) {
                player.sendMessage(new TextComponent(ChatColor.RED + "There's already a guild with that name!"));
                return;
            }

            ArrayList<Document> members = new ArrayList<Document>();
            members.add(new Document("uuid", player.getUniqueId()).append("role", "master"));

            Document newGuild = new Document("name", name)
                    .append("master", player.getUniqueId())
                    .append("options", new Document("tag", false))
                    .append("tag", null)
                    .append("members", members);

            newCol.insertOne(newGuild, new InsertOneOptions().bypassDocumentValidation(true));

            Core.getGuildManager().createGuild(name, player.getUniqueId(), members, null);

            profile.setGuild(player.getUniqueId());

            api.update("guild", player.getUniqueId());

            player.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "" +
                    "&b---------------------------------------------------\n" +
                    "&eYou created the guild &6" + name + "&6!\n" +
                    "&b---------------------------------------------------"
            )));

        }

        if(args[0].equalsIgnoreCase("disband")) {

            if(profile.getGuild() == null) {
                player.sendMessage(new TextComponent(ChatColor.RED + "You must be in a guild to do that!"));
                return;
            }

            if(!profile.getGuild().equals(player.getUniqueId())) {
                player.sendMessage(new TextComponent(ChatColor.RED + "You need to be the Guild Master to disband!"));
                return;
            }

            if(Core.getDisbandRequests().contains(player.getUniqueId().toString())) {

                Guild guild = Core.getGuildManager().getGuild(player.getUniqueId());

                player.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "" +
                        "&b---------------------------------------------------\n" +
                        "&eYou disbanded the guild!\n" +
                        "&b---------------------------------------------------"
                )));

                Core.getMongo().getDatabaseNew().getCollection("profiles")
                        .findOneAndUpdate(new Document("uniqueId", player.getUniqueId()),
                                new Document("$set", new Document("guild", null)));
                System.out.println("REMOVED OWNER");
                profile.setGuild(null);

                ArrayList<Document> members = guild.getMembers();

                for(Document member : members) {
                    UUID uuid_m = (UUID) member.get("uuid");
                    Core.getMongo().getDatabaseNew().getCollection("profiles")
                            .findOneAndUpdate(new Document("uniqueId", uuid_m),
                                    new Document("$set", new Document("guild", null)));
                    if(Core.getPlugin().getProxy().getPlayer(uuid_m) != null) {
                        Core.getPpManager().getProfile(uuid_m).setGuild(null);
                        Core.getPlugin().getProxy().getPlayer(uuid_m).sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "" +
                                "&b---------------------------------------------------\n" +
                                "&eThe guild was disbanded!\n" +
                                "&b---------------------------------------------------"
                        )));
                    }
                }

                Core.getGuildManager().getGuilds().remove(guild);
                Core.getDisbandRequests().remove(uuid.toString());

                BasicDBObject query = new BasicDBObject("master", player.getUniqueId());
                collection.remove(query);

                return;
            }

            Core.getDisbandRequests().add(uuid.toString());

            TextComponent message = new TextComponent("Are you sure you want to disband your guild? Type /g disband again to accept. You have 30 seconds!");
            message.setColor(ChatColor.RED);
            player.sendMessage(message);

            Core.getPlugin().getProxy().getScheduler().schedule(Core.getPlugin(), new Runnable() {
                public void run() {
                    if(Core.getDisbandRequests().contains(uuid.toString())) {
                        Core.getDisbandRequests().remove(uuid.toString());
                        Core.getPlugin().getProxy().getPlayer(uuid).sendMessage(new TextComponent(ChatColor.RED + "The disband command expired!"));
                    }
                }
            }, 30, TimeUnit.SECONDS);

        }

        if(args[0].equalsIgnoreCase("chat")) {
            if(profile.getGuild() == null) {
                player.sendMessage(new TextComponent(ChatColor.RED + "You must be in a guild to do that!"));
                return;
            }

            if(args.length < 2) {
                player.sendMessage(new TextComponent(ChatColor.RED + "/g chat <message>"));
                return;
            }

            StringBuilder builder = new StringBuilder();
            for(int i = 1; i < args.length; i++) {
                builder.append(args[i]).append(" ");
            }
            String message = builder.toString().trim();

            ArrayList<Document> members = Core.getGuildManager().getGuild(profile.getGuild()).getMembers();
            for(Document member : members) {
                UUID uuid_member = (UUID) member.get("uuid");
                if(Core.getPlayer(uuid_member) != null) {
                    Core.getPlayer(uuid_member).sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&',
                            "&2Guild > " + profile.getPrefix() + " " + player.getName() + "&f: " + message
                    )));
                }
            }
            return;
        }

        if(args[0].equalsIgnoreCase("tag")) {

            if(profile.getGuild() == null) {
                TextComponent message = new TextComponent("You must be in a guild to do that!");
                message.setColor(ChatColor.RED);
                player.sendMessage(message);
                return;
            }

            if(!profile.getGuild().equals(player.getUniqueId())) {
                player.sendMessage(new TextComponent(ChatColor.RED + "You must be the guild master to do that!"));
                return;
            }

            if(args.length < 2) {
                player.sendMessage(new TextComponent(ChatColor.RED + "Please specify a tag"));
                return;
            }

            String s = args[1];
            Pattern p = Pattern.compile("[^a-zA-Z0-9]");
            boolean hasSpecialChar = p.matcher(s).find();

            if(hasSpecialChar && profile.getRank().getLevel() < 4) {
                player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "Guild tags must be alpha-numeric!"));
                return;
            }


            Guild guild = Core.getGuildManager().getGuild(player.getUniqueId());

            String tag;

            if(profile.getRank().equals(Rank.ADMIN)) {
                tag = args[1];
            } else {
                tag = args[1].toUpperCase();
            }

            guild.setTag(tag);

            Core.getMongo().getDatabaseNew().getCollection("guilds").findOneAndUpdate(new Document("master", player.getUniqueId()),
                    new Document("$set", new Document("tag", tag)));

            ArrayList<Document> members = Core.getGuildManager().getGuild(profile.getGuild()).getMembers();
            for(Document member : members) {
                UUID uuid_member = (UUID) member.get("uuid");
                if(Core.getPlayer(uuid_member) != null) {
                    Core.getPlayer(uuid_member).sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&',
                            "&b---------------------------------------------------\n"
                                    + "&eGuild Tag was set to &6[" + tag + "&6]&e!\n"
                                    + "&b---------------------------------------------------"
                    )));
                }
            }
            return;
        }

        if(args[0].equalsIgnoreCase("invite")) {

            if(profile.getGuild() == null) {
                player.sendMessage(new TextComponent(ChatColor.RED + "You must be in a guild to do that!"));
                return;
            }

            Guild guild = Core.getGuildManager().getGuild(profile.getGuild());

            if(!(guild.getMemberRank(player.getUniqueId()) >= 1)) {
                player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "You must be at least Officer on the guild to do that!"));
                return;
            }

            if(args.length < 2) {
                player.sendMessage(new TextComponent(ChatColor.RED + "Please specify a tag"));
                return;
            }

            final UUID target = Cache.getCachedUUID(args[1].toLowerCase());
            final String playerUUID = guild.getMaster().toString();

            if(target == null) {
                player.sendMessage(new TextComponent(ChatColor.RED + "Player not found."));
                return;
            }

            if(target.equals(player.getUniqueId())) {
                player.sendMessage(new TextComponent(ChatColor.RED + "You cannot invite yourself!"));
                return;
            }

            final String targetUUID = target.toString();

            if(Core.getPlayer(target) == null) {
                player.sendMessage(new TextComponent(ChatColor.RED + "That player is not online! (Offline invites soonTM)"));
                return;
            }

            if(Core.getInvites().containsKey(target.toString())) {
                player.sendMessage(new TextComponent(ChatColor.RED + "You already invited that person to your guild!"));
                return;
            }

            Core.getInvites().put(target.toString(), guild.getMaster().toString());

            Core.getPlayer(target).sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "" +
                    "&b---------------------------------------------------\n" +
                    "&eYou were invited to &6" + guild.getName() + "&e!\n" +
                    "&6Type &b/guild accept " + Cache.getCachedName(guild.getMaster()) + "\n" +
                    "&b---------------------------------------------------"
            )));

            player.sendMessage(TextComponent.fromLegacyText(ChatColor.AQUA + "-----------------------------------------------------\n"
            + ChatColor.YELLOW + "You invited " + Cache.getCachedName(target) + " to the guild! They have 5 minutes to accept!\n" +
                    ChatColor.AQUA + "-----------------------------------------------------"

            ));

            Core.getPlugin().getProxy().getScheduler().schedule(Core.getPlugin(), new Runnable() {
                public void run() {
                    if(Core.getInvites().containsKey(targetUUID)) {
                        Core.getPlayer(UUID.fromString(playerUUID)).sendMessage(new TextComponent(ChatColor.RED +
                                "The guild invitation for " + Cache.getCachedName(UUID.fromString(targetUUID)) + " expired!"));
                        if(Core.getPlayer(UUID.fromString(targetUUID)) != null) {
                            Core.getPlayer(UUID.fromString(targetUUID)).sendMessage(new TextComponent(ChatColor.RED + "The guild invitation expired!"));
                        }
                        Core.getInvites().remove(targetUUID);
                        return;
                    }
                    Core.getInvites().remove(targetUUID);
                }
            }, 5, TimeUnit.MINUTES);
        }

        if(args[0].equalsIgnoreCase("accept")) {

            if(profile.getGuild() != null) {
                if(profile.getGuild().equals(player.getUniqueId())) {
                    player.sendMessage(new TextComponent(ChatColor.RED + "You already own a guild!"));
                    return;
                }
                player.sendMessage(new TextComponent(ChatColor.RED + "You're already in a guild!"));
                return;
            }

            if(args.length < 2) {
                player.sendMessage(new TextComponent(ChatColor.RED + "/g accept name"));
                return;
            }

            if(!Core.getInvites().containsKey(player.getUniqueId().toString())) {
                player.sendMessage(new TextComponent(ChatColor.RED + "You have not been invited to a guild!"));
                return;
            }

            Guild guild = Core.getGuildManager().getGuild(UUID.fromString(Core.getInvites().get(player.getUniqueId().toString())));
            ArrayList<Document> members = guild.getMembers();

            members.add(new Document("uuid", player.getUniqueId()).append("role", "member"));

            Core.getMongo().getDatabaseNew().getCollection("guilds").findOneAndUpdate(
                    new Document("master", UUID.fromString(Core.getInvites().get(player.getUniqueId().toString()))),
                    new Document("$set", new Document("members", members)));
            Core.getMongo().getDatabaseNew().getCollection("profiles").findOneAndUpdate(new Document("uniqueId", player.getUniqueId()),
                    new Document("$set", new Document("guild", guild.getMaster()))
            );

            player.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&',
                    "&b---------------------------------------------------\n" +
                            "&eYou joined &6" + guild.getName() + "&6!\n" +
                            "&b---------------------------------------------------"
                    )));

            for(Document member : guild.getMembers()) {
                UUID memberUUID = (UUID) member.get("uuid");
                if(Core.getPlayer(memberUUID) != null) {
                    if(memberUUID.equals(player.getUniqueId())) {
                    } else {
                        Core.getPlayer(memberUUID).sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "" +
                                "&b---------------------------------------------------\n" +
                                profile.getPrefix() + " " + player.getName() + " &ejoined the guild!\n" +
                                "&b---------------------------------------------------"
                        )));
                    }
                }
            }

            Core.getInvites().remove(player.getUniqueId().toString());

            guild.setMembers(members);
            profile.setGuild(guild.getMaster());
            return;
        }

        if(args[0].equalsIgnoreCase("list")) {
            if(profile.getGuild() == null) {
                player.sendMessage(new TextComponent(ChatColor.RED + "You must be in a guild to do that!"));
                return;
            }

            Guild guild = Core.getGuildManager().getGuild(profile.getGuild());
            int online = 0;
            ArrayList<Document> members = guild.getMembers();
            StringBuilder sb = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();
            for(int i = 1; i < members.size(); i++) {
                Document member = members.get(i);
                UUID uuid_member = (UUID) member.get("uuid");
                if(member.getString("role").equalsIgnoreCase("member")) {
                    if(Core.getPlayer(uuid_member) != null) {
                        sb.append(Cache.getCachedPrefix(uuid_member) + " " + Cache.getCachedName(uuid_member) + ChatColor.GREEN + " •  ");
                        online++;
                    } else {
                        sb.append(Cache.getCachedPrefix(uuid_member) + " " + Cache.getCachedName(uuid_member) + ChatColor.RED + " •  ");
                    }
                } else {
                    if(Core.getPlayer(uuid_member) != null) {
                        sb2.append(Cache.getCachedPrefix(uuid_member) + " " + Cache.getCachedName(uuid_member) + ChatColor.GREEN + " •  ");
                        online++;
                    } else {
                        sb2.append(Cache.getCachedPrefix(uuid_member) + " " + Cache.getCachedName(uuid_member) + ChatColor.RED + " •  ");
                    }
                }

            }
            player.sendMessage(new TextComponent(ChatColor.AQUA + "-----------------------------------------------------\n"
            + ChatColor.GOLD + "Guild Name: " + guild.getName() + "\n\n" +
                    ChatColor.GREEN + "                         -- Guild Master --                 "
            ));



            player.sendMessage(new TextComponent(Core.getPlayer(guild.getMaster()) == null ? ChatColor.translateAlternateColorCodes('&', Cache.getCachedPrefix(guild.getMaster())) + " " + Cache.getCachedName(guild.getMaster()) + ChatColor.RED + " •" : ChatColor.translateAlternateColorCodes('&', Cache.getCachedPrefix(guild.getMaster()) + " " + Cache.getCachedName(guild.getMaster()) + ChatColor.GREEN + " •")));
            if(Core.getPlayer(guild.getMaster()) != null) {
                online++;
            }
            player.sendMessage(new TextComponent(ChatColor.GREEN + "                        -- Guild Officers --                 "));
            player.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', sb2.toString())));
            player.sendMessage(new TextComponent(ChatColor.GREEN + "                        -- Guild Members --                 "));
            player.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', sb.toString())));
            player.sendMessage(new TextComponent("\n"));
            player.sendMessage(new TextComponent(ChatColor.YELLOW + "Total Members: " + ChatColor.GREEN + guild.getMembers().size()));
            player.sendMessage(new TextComponent(ChatColor.YELLOW + "Online Members: " + ChatColor.GREEN + online));
            player.sendMessage(new TextComponent(ChatColor.AQUA + "-----------------------------------------------------"));
            return;
        }

        if(args[0].equalsIgnoreCase("promote")) {
            if(profile.getGuild() == null) {
                player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "You must be in a guild to do that!"));
                return;
            }

            if(!profile.getGuild().equals(player.getUniqueId())) {
                player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "You must be the guild master to do that!"));
                return;
            }

            if(args.length < 2) {
                player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "Please specify a member of the guild!"));
                return;
            }

            if(Cache.getCachedUUID(args[1].toLowerCase()) == null) {
                player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "That player does not exist!"));
                return;
            }

            UUID uuid_member = Cache.getCachedUUID(args[1].toLowerCase());
            Guild guild = Core.getGuildManager().getGuild(profile.getGuild());
            if(guild.getMember(uuid_member) == null) {
                player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "That player is not in the guild!"));
                return;
            }
            Document member = guild.getMember(uuid_member);

            if(player.getUniqueId().equals(uuid_member)) {
                player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "You cannot promote yourself!"));
                return;
            }

            if(member.getString("role").equalsIgnoreCase("officer")) {
                player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "That player is already an officer!"));
                return;
            }

            ArrayList<Document> members = guild.getMembers();
            members.remove(member);
            member.put("role", "officer");
            members.add(member);

            Core.getMongo().getDatabaseNew().getCollection("guilds").findOneAndUpdate(new Document("master", guild.getMaster()),
                    new Document("$set", new Document("members", members)));

            guild.sendMessageFormatted(ChatColor.translateAlternateColorCodes('&', Cache.getCachedPrefix(uuid_member) + " " + Cache.getCachedName(uuid_member) + "&e was promoted from &6Member &eto &6Officer."));

            return;
        }

        if(args[0].equalsIgnoreCase("demote")) {
            if(profile.getGuild() == null) {
                player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "You must be in a guild to do that!"));
                return;
            }

            if(!profile.getGuild().equals(player.getUniqueId())) {
                player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "You must be the guild master to do that!"));
                return;
            }

            if(args.length < 2) {
                player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "Please specify a member of the guild!"));
                return;
            }

            if(Cache.getCachedUUID(args[1].toLowerCase()) == null) {
                player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "That player does not exist!"));
                return;
            }

            UUID uuid_member = Cache.getCachedUUID(args[1].toLowerCase());

            Guild guild = Core.getGuildManager().getGuild(profile.getGuild());
            if(guild.getMember(uuid_member) == null) {
                player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "That player is not in the guild!"));
                return;
            }

            if(player.getUniqueId().equals(uuid_member)) {
                player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "You cannot demote yourself!"));
                return;
            }

            Document member = guild.getMember(uuid_member);

            if(member.getString("role").equalsIgnoreCase("member")) {
                player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "That player is already a member!"));
                return;
            }

            ArrayList<Document> members = guild.getMembers();
            members.remove(member);
            member.put("role", "member");
            members.add(member);

            Core.getMongo().getDatabaseNew().getCollection("guilds").findOneAndUpdate(new Document("master", guild.getMaster()),
                    new Document("$set", new Document("members", members)));

            guild.sendMessageFormatted(ChatColor.translateAlternateColorCodes('&', Cache.getCachedPrefix(uuid_member) + " " + Cache.getCachedName(uuid_member) + "&e was demoted from &6Officer &eto &6Member."));
            return;
        }

        if(args[0].equalsIgnoreCase("kick")) {

            if(profile.getGuild() == null) {
                player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "You must be in a guild to do that!"));
                return;
            }

            Guild guild = Core.getGuildManager().getGuild(profile.getGuild());

            if(!(guild.getMemberRank(player.getUniqueId()) >= 1)) {
                player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "You must be the guild master to do that!"));
                return;
            }

            if(args.length < 2) {
                player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "Please specify a member of the guild."));
                return;
            }

            if(Cache.getCachedUUID(args[1].toLowerCase()) == null) {
                player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "That player does not exist!"));
                return;
            }

            UUID uuid_member = Cache.getCachedUUID(args[1].toLowerCase());

            if(guild.getMember(uuid_member) == null) {
                player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "That player is not in the guild!"));
                return;
            }

            if(player.getUniqueId().equals(uuid_member)) {
                player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "You cannot kick yourself!"));
                return;
            }

            Document member = guild.getMember(uuid_member);

            if(member.getString("role").equalsIgnoreCase("officer")) {
                if(guild.getMemberRank(player.getUniqueId()) == 1) {
                    player.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "Only the guild master can kick officers!"));
                    return;
                }
            }

            ArrayList<Document> members = guild.getMembers();
            members.remove(member);

            Core.getMongo().getDatabaseNew().getCollection("guilds").findOneAndUpdate(new Document("master", guild.getMaster()),
                    new Document("$set", new Document("members", members)));
            Core.getMongo().getDatabaseNew().getCollection("profiles").findOneAndUpdate(new Document("uniqueId", uuid_member),
                    new Document("$set", new Document("guild", null)));

            if(Core.getPlayer(uuid_member) != null) {
                Core.getPlayer(uuid_member).sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&',
                        "You were kicked from the guild!")));
                Core.getPpManager().getProfile(uuid_member).setGuild(null);
            }


            guild.sendMessageFormatted(ChatColor.translateAlternateColorCodes('&', Cache.getCachedPrefix(uuid_member) + " " + Cache.getCachedName(uuid_member) + " &ewas kicked from the guild by " + Cache.getCachedPrefix(player.getUniqueId()) + " " + player.getName() + "."));
            return;
        }

    }


}

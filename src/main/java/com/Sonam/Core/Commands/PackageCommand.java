package com.Sonam.Core.Commands;

import com.Sonam.Core.Core;
import com.Sonam.Core.Profiler.ProfileApi;
import com.Sonam.Core.Profiler.ProxyProfile;
import com.Sonam.Core.Profiler.Rank;
import com.Sonam.Core.Utils.Cache;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.bson.Document;

import java.util.ArrayList;
import java.util.UUID;

public class PackageCommand extends Command {

    String label;

    public PackageCommand(String label) {
        super(label);
        this.label = label;
    }

    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) sender;
        ProxyProfile profile = Core.getPpManager().getProfile(player.getUniqueId());

        if(profile == null) {
            player.sendMessage(new TextComponent(ChatColor.RED + "Your profile might be broken, please relog."));
            return;
        }

        if(profile.getRank().equals(Rank.ADMIN) || profile.getPackages().contains("op")) {

            if(args.length < 3) {
                player.sendMessage(new TextComponent(ChatColor.RED + "/" + label + " <add|rm> <player> <package>"));
                return;
            }

            UUID uuid = Cache.getCachedUUID(args[1].toLowerCase());

            ProfileApi api = new ProfileApi(uuid);

            if(!api.exists()) {
                player.sendMessage(new TextComponent(ChatColor.RED + "Player not found."));
                return;
            }

            ArrayList<String> packages = (ArrayList<String>) api.get("packages");

            String output_msg;

            if(Core.getPackages().contains(args[2].toUpperCase())) {

                if(args[0].equalsIgnoreCase("rm")) {
                    if(!packages.contains(args[2])) {
                        player.sendMessage(new TextComponent(ChatColor.RED + "That player does not have the \'" + args[2].toLowerCase() + "\' package."));
                        return;
                    }
                    packages.remove(args[2].toLowerCase());
                    output_msg = "Removed \'" + args[2].toLowerCase() + "\' package from " + args[1];
                } else if (args[0].equalsIgnoreCase("add")) {
                    if(packages.contains(args[2])) {
                        player.sendMessage(new TextComponent(ChatColor.RED + "That player already has the \'" + args[2].toLowerCase() + "\' package."));
                        return;
                    }
                    packages.add(args[2].toLowerCase());
                    output_msg = "Added \'" + args[2].toLowerCase() + "\' package to " + args[1];
                } else {
                    player.sendMessage(new TextComponent(ChatColor.RED + "Invalid Arguments"));
                    return;
                }

                Core.getMongo().getDatabaseNew().getCollection("profiles")
                        .findOneAndUpdate(new Document("uniqueId", uuid), new Document("$set", new Document("packages", packages)));
                player.sendMessage(new TextComponent(ChatColor.GREEN + output_msg));

                if(Core.getPlayer(uuid) != null) {
                    Core.getPpManager().getProfile(uuid).setPackages(packages);
                    return;
                }
                return;
            }
            player.sendMessage(new TextComponent(ChatColor.RED + "That package is not valid."));

            return;
        }
        player.sendMessage(new TextComponent(ChatColor.RED + "You do not have permission to do this."));

    }

}

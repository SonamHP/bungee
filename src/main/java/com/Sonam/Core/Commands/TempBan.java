package com.Sonam.Core.Commands;

import com.Sonam.Core.Core;
import com.Sonam.Core.Profiler.ProfileApi;
import com.Sonam.Core.Profiler.ProxyProfile;
import com.Sonam.Core.Utils.Cache;
import com.Sonam.Core.Utils.Utils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.bson.Document;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.UUID;

import static net.md_5.bungee.api.ChatColor.*;

public class TempBan extends Command {

    public TempBan() {
        super("tempban");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        ProxiedPlayer player = (ProxiedPlayer) sender;
        ProxyProfile profile = Core.getPpManager().getProfile(player.getUniqueId());

        if(profile.getRank().getLevel() >= 3) {
            if(args.length < 3) {
                player.sendMessage(new TextComponent(RED + "/tempban <name> <time> <reason>"));
                return;
            }

            ProfileApi api = new ProfileApi(Cache.getCachedUUID(args[0].toLowerCase()));

            if(!api.exists()) {
                player.sendMessage(new TextComponent(RED + "That player does not exist!"));
                return;
            }

            ArrayList<Document> bans = (ArrayList<Document>) api.get("bans");

            if(!bans.isEmpty()) {
                if(bans.get(bans.size() - 1).get("active").equals(true)) {
                    player.sendMessage(new TextComponent(ChatColor.RED + "That player is already banned."));
                    return;
                }
            }

            UUID targetUUID = Cache.getCachedUUID(args[0].toLowerCase());

            StringBuilder builder = new StringBuilder();
            for(int i = 2; i < args.length; i++) {
                builder.append(args[i]).append(" ");
            }
            String reason = builder.toString().trim();

            Document init = new Document("bantype", 2).append("bannedBy", player.getUniqueId())
                    .append("reason", reason).append("active", true).append("start", DateTime.now().toString()).append("unbanReason", null).append("unbannedBy", null);

            String initTime = args[1];

            char[] array = initTime.toCharArray();
            char stamp = array[array.length - 1];
            StringBuilder sb = new StringBuilder(initTime);
            sb.deleteCharAt(initTime.length() - 1);
            int finalTime = Integer.parseInt(sb.toString().trim());

            String brs = null;

            switch (stamp) {
                case 'm':
                    init.append("end", DateTime.now().plusMinutes(finalTime).toString());
                    brs = "0D:0H:" + finalTime + "M";
                    init.append("time", brs);
                    break;
                case 'M':
                    init.append("end", DateTime.now().plusMinutes(finalTime).toString());
                    brs = "0D:0H:" + finalTime + "M";
                    init.append("time", brs);
                    break;
                case 'h':
                    init.append("end", DateTime.now().plusHours(finalTime).toString());
                    brs = "0D:" + finalTime + "H:0M";
                    init.append("time", brs);
                    break;
                case 'H':
                    init.append("end", DateTime.now().plusHours(finalTime).toString());
                    brs = "0D:" + finalTime + "H:0M";
                    init.append("time", brs);
                    break;
                case 'd':
                    init.append("end", DateTime.now().plusDays(finalTime).toString());
                    brs = finalTime + "D:0H:0M";
                    init.append("time", brs);
                    break;
                case 'D':
                    init.append("end", DateTime.now().plusDays(finalTime).toString());
                    brs = finalTime + "D:0H:0M";
                    init.append("time", brs);
                    break;
                default:
                    player.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "&cPlease input a correct timestamp. Ex: 7D")));
                    return;
            }


            bans.add(init);

            Core.getMongo().getDatabaseNew().getCollection("profiles")
                    .findOneAndUpdate(new Document("uniqueId", targetUUID),
                            new Document("$set", new Document("bans", bans)));

            if(Core.getPlayer(targetUUID) != null) {
                Core.getPlayer(targetUUID).disconnect(new TextComponent(RED + "You have been banned from the server for " + reason + ". \nYour suspension will end in " + brs));
                Utils.sendStaffChat(WHITE + player.getName() + " kicked player \'" +
                        Cache.getCachedName(targetUUID) + ". Reason: " + reason + ".");

            }
            player.sendMessage(new TextComponent(YELLOW + "Temporarily banned \'" + Cache.getCachedName(targetUUID) + "\'"));
            Utils.sendStaffChat(WHITE + player.getName() + " temporarily banned player \'" +
                    Cache.getCachedName(targetUUID) + "\' for " + brs + ". Reason: " + reason + ".");

            return;
        }
        player.sendMessage(new TextComponent(RED + "You do not have permission to do this!"));

    }
}

package com.Sonam.Core.Commands;

import com.Sonam.Core.Core;
import com.Sonam.Core.Guild.Guild;
import com.Sonam.Core.Profiler.P_API;
import com.Sonam.Core.Profiler.ProfileApi;
import com.Sonam.Core.Profiler.ProxyProfile;
import com.Sonam.Core.Profiler.Rank;
import com.Sonam.Core.Utils.Cache;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.bson.Document;

import java.util.ArrayList;
import java.util.UUID;

public class Userinfo extends Command {

    public Userinfo() {
        super("userinfo");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        DBCollection collection = Core.getMongo().getDatabase().getCollection("profiles");

        ProxiedPlayer player = (ProxiedPlayer) sender;

        ProxyProfile profile = Core.getPpManager().getProfile(player.getUniqueId());

        if(profile.equals(null)) {
            return;
        }

        if(profile.getRank().isStaff()) {


            if(args.length == 0) {
                player.sendMessage(new TextComponent(ChatColor.RED + "/userinfo <player>"));
                return;
            }

            UUID uuid = Cache.getCachedUUID(args[0].toLowerCase());

            DBCursor cursor = collection.find(new BasicDBObject("uniqueId", uuid));

            if(cursor.hasNext()) {
                P_API api2 = new P_API(uuid);
                ProfileApi api = new ProfileApi(uuid);

                String prefix, currentServer;

                ArrayList<Document> bans = (ArrayList<Document>) api.get("bans");
                ArrayList<Document> mutes = (ArrayList<Document>) api.get("mutes");

                if(api.getString("prefix") == null) {
                    if(api.getRank().equals(Rank.NORMAL)) {
                        prefix = ChatColor.translateAlternateColorCodes('&', api.getPackageRank().getPrefix());
                    } else {
                        prefix = ChatColor.translateAlternateColorCodes('&', api.getRank().getPrefix());
                    }
                }  else {
                    prefix = ChatColor.translateAlternateColorCodes('&', api.getString("prefix"));
                }

                if(Core.getPlugin().getProxy().getPlayer(uuid) != null) {
                    currentServer = Core.getPlugin().getProxy().getPlayer(uuid).getServer().getInfo().getName() +
                    "\n&6Server Type: &f" + Core.getServerManager().getInstanceType(Core.getPlugin().getProxy().getPlayer(uuid).getServer().getInfo().getName());
                } else {
                    currentServer = "&7&oOffline";
                }

                String guildName;

                if(api.getGuildID() == null) {
                    guildName = "&7None";
                } else {
                    Guild guild = Core.getGuildManager().getGuild(api.getGuildID());
                    guildName = "&f" + guild.getName();
                }

                player.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&',
                        "&6--- Info about " + prefix + " " + api.getString("username") + " &6---\n"
                        + "&6Most Recent Name: &f" + api.getString("username") + "\n"
                        + "&6Rank: &f" + api.getRank().name() + "\n"
                        + "&6PackageRank: &f" + api.getPackageRank().name() + "\n"
                        + "&6Network Level: &f0\n"
                        + "&6Guild: " + guildName + "\n"
                        + "&6Current Server: &f" + currentServer + "\n"
                        + "&6Packages: &f" + api.get("packages") + "\n"
                        + "&6Bans: &f" + bans.size() + "\n"
                        + "&6Mutes: &f" + mutes.size()
                )));

                return;
            }

            player.sendMessage(new TextComponent(ChatColor.RED + "No player by that name was found!"));

            return;
        }

        player.sendMessage(new TextComponent(ChatColor.RED + "You do not have permission to use this command."));

    }
}

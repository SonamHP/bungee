package com.Sonam.Core.Commands;

import com.Sonam.Core.Core;
import com.Sonam.Core.Profiler.P_API;
import com.Sonam.Core.Profiler.ProxyProfile;
import com.Sonam.Core.Profiler.Rank;
import com.Sonam.Core.Utils.Cache;
import com.Sonam.Core.Utils.Utils;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.plugin.Command;
import org.json.JSONObject;

import java.util.UUID;

public class SetRank extends Command {

    public SetRank() {
        super("rank");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        DBCollection collection = Core.getMongo().getDatabase().getCollection("profiles");

        if (!(sender instanceof ProxiedPlayer)) {
            if (args.length < 2) {
                Core.getPlugin().getProxy().getLogger().info("/rank <player> <rank>");
                return;
            }

            if (!Core.getRanks().contains(args[1].toUpperCase())) {
                Core.getPlugin().getProxy().getLogger().info("That's not a valid rank!");
                return;
            }

            UUID uuid = Cache.getCachedUUID(args[0].toLowerCase());

            if(uuid == null) {
                Core.getPlugin().getProxy().getLogger().info("Player not found!");
                return;
            }

            BasicDBObject query = new BasicDBObject("uniqueId", uuid);
            DBCursor cursor = collection.find(query);

            if (cursor.hasNext()) {
                P_API api = new P_API(uuid);

                for (Rank rank : Rank.values()) {
                    if (rank.name().equalsIgnoreCase(args[1].toUpperCase())) {

                        BasicDBObject update = new BasicDBObject("rank", args[1].toUpperCase());

                        collection.update(query, new BasicDBObject("$set", update));

                        Utils.sendStaffChat(ChatColor.translateAlternateColorCodes('&', "&4[SYSTEM]&f set rank of " + Cache.getCachedName(uuid) + " to " + rank.getSetter()));

                        if (Core.getPlugin().getProxy().getPlayer(uuid) != null) {
                            Core.getPlugin().getProxy().getPlayer(uuid).sendMessage(new TextComponent(ChatColor.GREEN + "You are now " + rank.name()));
                            ProxyProfile profile = Core.getPpManager().getProfile(uuid);
                            if(profile == null) {
                                Core.getPpManager().createProfile(uuid);
                            }
                            try {
                                Thread.sleep(500);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                            profile.setRank(rank);
                            try {
                                Thread.sleep(500);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                            profile.setPrefix(ChatColor.translateAlternateColorCodes('&', api.getPrefix()));
                        }

                        if(rank.equals(Rank.NORMAL)) {
                            api.updateCachePrefix(api.getPackageRank().getPrefix());
                            return;
                        }

                        api.updateCachePrefix(rank.getPrefix());

                    }
                }

            }
            return;
        }

        ProxiedPlayer player = (ProxiedPlayer) sender;

        ProxyProfile profile = Core.getPpManager().getProfile(player.getUniqueId());

        if (profile.getPackages() == null) {
            player.sendMessage(new TextComponent(ChatColor.RED + "You are not allowed to do this."));
            return;
        }

        if (args.length < 2) {
            player.sendMessage(new TextComponent(ChatColor.RED + "/rank <player> <rank>"));
            return;
        }

        if (!Core.getRanks().contains(args[1].toUpperCase())) {
            player.sendMessage(new TextComponent(ChatColor.RED + "That's not a valid rank!"));
            return;
        }

        if (profile.getRank().equals(Rank.ADMIN) || profile.getPackages().contains("op")) {
            UUID uuid = Cache.getCachedUUID(args[0].toLowerCase());

            if(uuid == null) {
                player.sendMessage(new TextComponent(ChatColor.RED + "Player not found!"));
                return;
            }

            BasicDBObject query = new BasicDBObject("uniqueId", uuid);
            DBCursor cursor = collection.find(query);

            if (cursor.hasNext()) {
                P_API api = new P_API(uuid);

                for (Rank rank : Rank.values()) {
                    if (rank.name().equalsIgnoreCase(args[1].toUpperCase())) {

                        BasicDBObject update = new BasicDBObject("rank", args[1].toUpperCase());

                        collection.update(query, new BasicDBObject("$set", update));

                        Utils.sendStaffChat(ChatColor.translateAlternateColorCodes('&', profile.getPrefix().replace("_", " ") + " " + profile.getUsername() + "&f set rank of " + Cache.getCachedName(uuid) + " to " + rank.getSetter()));
                        try {
                            JSONObject object = new JSONObject().append("username", "SYSTEM").append("message", "> *_" + player.getName() + " set rank of " + Cache.getCachedName(uuid) + " to " + rank.name() + "_*").append("uuid", player.getUniqueId().toString());
                            Core.getSocketIo().getSocket().emit("toSlack", object);
                        }catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        //This is very ugly code, will clean up.
                        if (Core.getPlugin().getProxy().getPlayer(uuid) != null) {
                            if(api.getPrefix() == null) {
                                Core.getPpManager().getProfile(uuid).setPrefix(rank.getPrefix());
                            }
                            if(!rank.isStaff() && Core.getStaff().contains(uuid)) {
                                Core.getStaff().remove(uuid);
                            } else if(rank.isStaff() && !Core.getStaff().contains(uuid)) {
                                Core.getStaff().add(uuid);
                            }
                            Core.getPlugin().getProxy().getPlayer(uuid).sendMessage(new TextComponent(ChatColor.GREEN + "You are now " + rank.name()));
                            String setRankCrossServer = "SETRANK " + uuid.toString() + " " + rank.name();
                            Server server = Core.getPlayer(uuid).getServer();
                            server.sendData("BungeeCord", setRankCrossServer.getBytes());
                        }

                        if (rank.equals(Rank.NORMAL)) {
                            api.updateCachePrefix(api.getPackageRank().getPrefix());
                            return;
                        }

                        api.updateCachePrefix(rank.getPrefix());

                    }
                }
                return;
            }

            player.sendMessage(new TextComponent(ChatColor.RED + "No player by that name was found!"));


        }
    }
}

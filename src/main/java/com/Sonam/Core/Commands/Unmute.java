package com.Sonam.Core.Commands;

import com.Sonam.Core.Core;
import com.Sonam.Core.Profiler.ProfileApi;
import com.Sonam.Core.Profiler.ProxyProfile;
import com.Sonam.Core.Utils.Cache;
import com.Sonam.Core.Utils.Utils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.bson.Document;

import java.util.ArrayList;
import java.util.UUID;

import static net.md_5.bungee.api.ChatColor.RED;

public class Unmute extends Command {

    public Unmute() {
        super("unmute");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        ProxiedPlayer player = (ProxiedPlayer) sender;
        ProxyProfile profile = Core.getPpManager().getProfile(player.getUniqueId());

        if(profile.getRank().getLevel() >= 2) {

            if (args.length < 1) {
                player.sendMessage(new TextComponent(RED + "/unmute <player>"));
                return;
            }

            ProfileApi api = new ProfileApi(Cache.getCachedUUID(args[0].toLowerCase()));

            if (Cache.getCachedUUID(args[0].toLowerCase()) == null) {
                player.sendMessage(new TextComponent(RED + "That player does not exist!"));
                return;
            }

            if (!api.exists()) {
                player.sendMessage(new TextComponent(RED + "That player does not exist!"));
                return;
            }

            UUID targetUUID = Cache.getCachedUUID(args[0].toLowerCase());

            ArrayList<Document> mutes = (ArrayList<Document>) api.get("mutes");

            if(mutes.isEmpty()) {
                player.sendMessage(new TextComponent(ChatColor.RED + "That player is not muted!"));
                return;
            }

            if(mutes.get(mutes.size() - 1).get("active").equals(false)) {
                player.sendMessage(new TextComponent(ChatColor.RED + "That player is not muted!"));
                return;
            }

            Document mute = mutes.get(mutes.size() - 1);
            mute.remove("active");
            mute.append("active", false);
            mutes.remove(mutes.size() -1);
            mutes.add(mute);

            Core.getMongo().getDatabaseNew().getCollection("profiles").findOneAndUpdate(
                    new Document("uniqueId", targetUUID), new Document("$set", new Document("mutes", mutes)));

            Utils.sendStaffChat(ChatColor.WHITE + player.getName() + " unmuted '" + Cache.getCachedName(targetUUID) + ".");

            if(Core.getPlayer(targetUUID) != null) {
                Core.getPlayer(targetUUID).sendMessage(new TextComponent(ChatColor.GREEN + "You were unmuted!"));
                Core.getPpManager().getProfile(targetUUID).setMuted(false);
            }

        }
    }
}

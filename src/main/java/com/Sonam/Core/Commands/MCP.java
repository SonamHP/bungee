package com.Sonam.Core.Commands;

import com.Sonam.Core.Core;
import com.Sonam.Core.Profiler.ProxyProfile;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.ArrayList;
import java.util.Map;

public class MCP extends Command {

    public MCP() {
        super("mcp");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        Map<String, ServerInfo> servers = Core.getPlugin().getProxy().getServers();
        ArrayList<ServerInfo> serverInfos = new ArrayList<ServerInfo>();

        if(!(sender instanceof ProxiedPlayer)) {
            for(Map.Entry<String, ServerInfo> entry : servers.entrySet()) {
                serverInfos.add(entry.getValue());
            }
            Core.getPlugin().getLogger().info(ChatColor.translateAlternateColorCodes('&',
                    "&c[MCP] &aLoading Servers..."));

            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Core.getPlugin().getLogger().info(ChatColor.translateAlternateColorCodes('&',
                    "&c[MCP] &aReloading Servers (" + serverInfos.size() + ")...."));
            for(ServerInfo serverInfo : serverInfos) {
                String data = "MCP reload";
                serverInfo.sendData("BungeeCord", data.getBytes());
            }
            return;
        }

        ProxiedPlayer player = (ProxiedPlayer) sender;
        ProxyProfile profile = Core.getPpManager().getProfile(player.getUniqueId());
    }
}

package com.Sonam.Core.Commands;

import com.Sonam.Core.Core;
import com.Sonam.Core.Profiler.ProxyProfile;
import com.Sonam.Core.Utils.Utils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.json.JSONObject;

public class StaffChat extends Command {

    public StaffChat() {
        super("s");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if(!(sender instanceof ProxiedPlayer)) {

            StringBuilder builder = new StringBuilder();
            for(int i = 0; i < args.length; i++) {
                builder.append(args[i]).append(" ");
            }
            String message = builder.toString().trim();

            Utils.sendStaffChat(ChatColor.translateAlternateColorCodes('&', "&4[SYSTEM]&f " + message));

            return;
        }

        ProxiedPlayer player = (ProxiedPlayer) sender;

        if(!Core.getStaff().contains(player.getUniqueId())) {
            player.sendMessage(new TextComponent(ChatColor.RED + "You do not have permission to do this!"));
            return;
        }

        if(args.length < 1) {
            player.sendMessage(new TextComponent(ChatColor.RED + "/s <message>"));
            return;
        }

        ProxyProfile profile = Core.getPpManager().getProfile(player.getUniqueId());

        StringBuilder builder = new StringBuilder();
        for(int i = 0; i < args.length; i++) {
            builder.append(args[i]).append(" ");
        }
        String message = builder.toString().trim();

        Utils.sendStaffChat(ChatColor.translateAlternateColorCodes('&', profile.getPrefix().replace("_", " ") + " " + profile.getUsername() + "&f: " + message));

        try {
            JSONObject object = new JSONObject().append("username", player.getName()).append("message", message).append("uuid", player.getUniqueId().toString());
            Core.getSocketIo().getSocket().emit("toSlack", object);

        }catch (Exception ex) {
            ex.printStackTrace();
        }


    }
}

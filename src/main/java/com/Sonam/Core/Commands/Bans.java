package com.Sonam.Core.Commands;

import com.Sonam.Core.Core;
import com.Sonam.Core.Profiler.ProfileApi;
import com.Sonam.Core.Profiler.ProxyProfile;
import com.Sonam.Core.Utils.Cache;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.bson.Document;

import java.util.ArrayList;
import java.util.UUID;

@SuppressWarnings("all")
public class Bans extends Command {

    public Bans() {
        super("bans");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        ProxiedPlayer player = (ProxiedPlayer) sender;
        ProxyProfile profile = Core.getPpManager().getProfile(player.getUniqueId());

        if(profile.getRank().getLevel() >= 3) {

            if(args.length < 1) {
                player.sendMessage(new TextComponent(ChatColor.RED + "/bans <player>"));
                return;
            }

            UUID target = Cache.getCachedUUID(args[0].toLowerCase());

            if(target == null) {
                player.sendMessage(new TextComponent(ChatColor.RED + "Player not found."));
                return;
            }

            ProfileApi api = new ProfileApi(target);

            ArrayList<Document> bans = (ArrayList<Document>) api.get("bans");

            if(bans.isEmpty()) {
                player.sendMessage(new TextComponent(ChatColor.YELLOW + "That person does not have any bans."));
                return;
            }

            StringBuilder bansMsg = new StringBuilder();
            for(int i = 0; i < bans.size(); i++) {
                Document ban = bans.get(i);
                if(ban.get("bantype").equals(1)) {
                    if(ban.get("bannedBy") instanceof String) {
                        bansMsg.append(i + 1 + ". PermaBan by " + ban.getString("bannedBy") + ". Reason: " + ban.getString("reason"));
                    } else {
                        bansMsg.append(i + 1 + ". PermaBan by " + Cache.getCachedName((UUID) ban.get("bannedBy")) + ". Reason: " + ban.getString("reason"));
                    }
                    if(ban.getString("unbanReason") != null) {
                        if(ban.get("unbannedBy") instanceof String) {
                            bansMsg.append(". Unbanned by " + ChatColor.GRAY + ban.getString("unbannedBy") + " " + ChatColor.YELLOW + "for " + ban.getString("unbanReason") + "\n");
                        } else {
                            bansMsg.append(". Unbanned by " + Cache.getCachedName((UUID) ban.get("unbannedBy")) + " for " + ban.getString("unbanReason") + "\n");
                        }
                    } else {
                        bansMsg.append("\n");
                    }
                } else {
                    if(ban.get("bannedBy") instanceof String) {
                        bansMsg.append(i + 1 + ". TempBan by " + ban.getString("bannedBy") + " for " + ban.getString("time") + ". Reason: " + ban.getString("reason"));
                    } else {
                        bansMsg.append(i + 1 + ". TempBan by " + Cache.getCachedName((UUID) ban.get("bannedBy")) + " for " + ban.getString("time") + ". Reason: " + ban.getString("reason"));
                    }
                    if(ban.getString("unbanReason") != null) {
                        if(ban.get("unbannedBy") instanceof String) {
                            bansMsg.append(". Unbanned by " + ChatColor.GRAY + ban.getString("unbannedBy") + " " + ChatColor.YELLOW + "for " + ban.getString("unbanReason") + "\n");
                        } else {
                            bansMsg.append(". Unbanned by " + Cache.getCachedName((UUID) ban.get("unbannedBy")) + " for " + ban.getString("unbanReason") + "\n");
                        }
                    } else {
                        bansMsg.append("\n");
                    }
                }
            }
            String finalMsg = bansMsg.toString().trim();

            TextComponent banss = new TextComponent(ChatColor.translateAlternateColorCodes('&', finalMsg));
            banss.setColor(ChatColor.YELLOW);

            player.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "&6--- Bans for " + Cache.getCachedName(target) + " (Page 1 of 1) ---")));
            player.sendMessage(banss);

            return;
        }
        player.sendMessage(new TextComponent(ChatColor.RED + "You do not have permission to do this!"));

    }
}

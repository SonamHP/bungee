package com.Sonam.Core.SocketListeners;

import com.Sonam.Core.Core;
import com.Sonam.Core.Profiler.ProfileApi;
import com.Sonam.Core.Utils.Cache;
import com.Sonam.Core.Utils.Utils;
import io.socket.emitter.Emitter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import org.bson.Document;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.UUID;

@SuppressWarnings("unchecked")
public class Punishment implements Emitter.Listener {

    public void call(Object... args) {
        JSONObject object = (JSONObject) args[0];
        try {

            if(Cache.getCachedUUID(object.getString("name").toLowerCase()) == null) {
                Core.getSocketIo().getSocket().emit("punishment_not_found", "Player not found!");
                return;
            }

            UUID uuid = Cache.getCachedUUID(object.getString("name").toLowerCase());
            ProfileApi api = new ProfileApi(uuid);
            ArrayList<Document> bans = (ArrayList<Document>) api.get("bans");

            if(object.getString("type").equalsIgnoreCase("ban")) {
                if(!bans.isEmpty()) {
                    Document ban = bans.get(bans.size() - 1);
                    if(ban.getBoolean("active").equals(true)) {
                        Core.getSocketIo().getSocket().emit("punishment_not_found", "That player is already banned!");
                        return;
                    }
                }
                Document newBan = new Document("bantype", 1).append("bannedBy", Cache.getCachedUUID(object.getString("nick").toLowerCase()))
                        .append("reason", object.getString("reason")).append("active", true).append("date", new LocalDate().toString());
                bans.add(newBan);
                Core.getMongo().getDatabaseNew().getCollection("profiles").findOneAndUpdate(new Document("uniqueId", uuid),
                        new Document("$set", new Document("bans", bans)));
                Utils.sendStaffChat(ChatColor.translateAlternateColorCodes('&', "&d[WEB] &f" +
                        object.getString("nick") + " banned player \'" + object.getString("name") + "\'. Reason: " + object.getString("reason")));

                if(Core.getPlayer(uuid) != null) {
                    Core.getPlayer(uuid).disconnect(new TextComponent(ChatColor.RED + "You have been banned from this server! Reason: " + object.getString("reason")));
                }
                return;
            }
            if(object.getString("type").equalsIgnoreCase("unban")) {
                if(bans.isEmpty()) {
                    Core.getSocketIo().getSocket().emit("punishment_not_found", "That player is not banned!");
                    return;
                }
                Document ban = bans.get(bans.size() - 1);
                if(ban.getBoolean("active").equals(false)) {
                    Core.getSocketIo().getSocket().emit("punishment_not_found", "That player is not banned!");
                    return;
                }
                ban.put("unbanReason", object.getString("reason"));
                if(ban.getInteger("bantype").equals(2)) {
                    ban.put("end", DateTime.now().toString());
                }
                ban.put("unbannedBy", Cache.getCachedUUID(object.getString("nick").toLowerCase()));
                ban.put("active", false);
                bans.remove(bans.size() - 1);
                bans.add(ban);
                Core.getMongo().getDatabaseNew().getCollection("profiles").findOneAndUpdate(new Document("uniqueId", uuid),
                        new Document("$set", new Document("bans", bans)));
                Utils.sendStaffChat(ChatColor.translateAlternateColorCodes('&', "&d[WEB] &f" +
                        object.getString("nick") + " unbanned player \'" + object.getString("name") + "\'. Reason: " + object.getString("reason")));
                return;
            }



        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(object);
    }

}
